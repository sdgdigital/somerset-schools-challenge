<?php

return [

    // "Admin"
    'admin'                  => 'admin',

    // options
    'option'                 => 'option',
    'options'                => 'options',
    'options-icon'           => 'fa fa-cogs fa-fw',

    // modes
    'mode'                   => 'mode',
    'modes'                  => 'modes',
    'modes-icon'             => 'fa fa-list fa-fw',

    // weather
    'weather'                => 'weather',
    'weather-icon'           => 'fa fa-bolt fa-fw',

    // competitions
    'competition'            => 'competition',
    'competitions'           => 'competitions',
    'competitions-icon'      => 'fa fa-calendar fa-fw',

    // local authorities
    'local-authority'        => 'local authority',
    'local-authorities'      => 'local authorities',
    'local-authorities-icon' => 'fa fa-map fa-fw',

    // schools
    'school'                 => 'school',
    'schools'                => 'schools',
    'schools-icon'           => 'fa fa-graduation-cap fa-fw',

    // users
    'user'                   => 'teacher',
    'users'                  => 'teachers',
    'users-icon'             => 'fa fa-user fa-fw',

    // classrooms
    'classroom'              => 'class',
    'classrooms'             => 'classes',
    'classrooms-icon'        => 'fa fa-users fa-fw',

    // trips
    'trip'                   => 'journey',
    'trips'                  => 'journeys',
    'trips-icon'             => 'fa fa-car fa-fw',

    // registration
    'register'               => 'register',
    'login'                  => 'login',
    'logout'                 => 'logout',

    // pages
    'page'                   => 'page',
    'pages'                  => 'pages',
    'pages-icon'             => 'fa fa-file fa-fw',

    // teacher
    'teacher'                => 'teacher',

    // reports
    'report'                 => 'report',
    'reports'                => 'reports',
    'reports-icon'           => 'fa fa-print fa-fw',

];
