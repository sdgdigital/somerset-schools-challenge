import 'babel-polyfill'
import Vue from 'vue'
//import store from './index'
//import shop from '../api/shop'
import * as types from './mutation-types'

/*export const addToCart = ({ commit }, product) => {
  if (product.inventory > 0) {
    commit(types.ADD_TO_CART, {
      id: product.id
    })
  }
}

export const checkout = ({ commit, state }, products) => {
  const savedCartItems = [...state.cart.added]
  commit(types.CHECKOUT_REQUEST)
  shop.buyProducts(
    products,
    () => commit(types.CHECKOUT_SUCCESS),
    () => commit(types.CHECKOUT_FAILURE, { savedCartItems })
  )
}

export const getAllProducts = ({ commit }) => {
  shop.getProducts(products => {
    commit(types.RECEIVE_PRODUCTS, { products })
  })
}*/

export const getData = ({ dispatch }, data) => {
  console.log(data);
  Vue.http.get(data.child)
    .then(response => {
      /*let pageData = response.json()

      pageData.map(pageData => {
        pageData.checked = false 
        return pageData
      })*/

      dispatch(types.STORE_DATA, response)
    })
}

/*export const postData = ({ dispatch }, data) => {
  Vue.http.post(data.child, data.data)
    .then(response => {
      let pageData = response.json()

      pageData.map(pageData => {
        pageData.checked = false
        return pageData
      })

      types.dispatch('STORE_DATA', response)
    })
}*/

/*loadData (context, data) {
  context.commit('loadData');
  
  var response = {
    modes: [
      {mode: 'walk', title: 'walk' ,counter: 0},
      {mode: 'bus', title: 'bus' ,counter: 0},
      {mode: 'bike', title: 'bike' ,counter: 0},
      {mode: 'car', title: 'car' ,counter: 0},
      {mode: 'train', title: 'train' ,counter: 0},
      {mode: 'scooter', title: 'scooter' ,counter: 0},
      {mode: 'share', title: 'share' ,counter: 0},
      {mode: 'park', title: 'park & stride' ,counter: 0},
      {mode: 'field', title: 'field lap' ,counter: 0}
    ],
    pupil_total: 2,//32,
    pupil_in: 0,
    weather:0,
    points:0
  };

  Vue.http.get(data.child).success(function(response) {
    console.log(response);
    //this.$set('response', response);
  }).error(function(error) {
    console.log(error);
  });
  
  context.commit('storeData', response);
  console.log("getData");
}*/
