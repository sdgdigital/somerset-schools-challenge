import Vue from 'vue'
import Vuex from 'vuex'
// import VueRouter from 'vue-router'

// import store 'session' module
// import * as session from './modules/session'
import * as types from './mutation-types'

/* Initialise the app to use Vuex */
Vue.use(Vuex);


/* Plugin to persist state */
const statePersist = store => { // -- NOT USED
  // called when the store is initialized
  console.log("PLUGIN Initialized");

  // called after every mutation.
  // The mutation comes in the format of { type, payload }.
  store.subscribe((mutation, state) => {

    // We only save the state
   /* if (mutation.type == 'save-answers'){
      //localStorage['sdgsurvey_state'] = JSON.stringify(state);
      console.log("mutation: save-answers")
    }*/

  })
}


export default new Vuex.Store({
  state: {
    loaded: false,
    pageData: {},
    popupShow: false,
    popupCallBack: null,
    popupData: {},
    jumpTo: "",
    jump: false,
    publicSite: false
  },

  getters: {
    loaded: state => {
      return state.loaded
    },
    pageData: state => {
      return state.pageData
    },
    popupShow: state => {
      return state.popupShow
    },
    popupCallBack: state => {
      return state.popupCallBack
    },
    popupData: state => {
      return state.popupData
    },
    jumpTo: state => {
      return state.jumpTo
    },
    jump: state => {
      return state.jump
    },
    publicSite: state => {
      return state.publicSite
    }
  },

  mutations: {
    loading(state, response) {
      state.loaded = false;
    },
    storeData (state, response) {
      state.pageData = response;
      state.loaded = true;
    },
    popupShow(state, response) {
      state.popupShow = response;
    },
    popupData(state, response) {
      state.popupData = response;
    },
    popupCallBack(state, response) {
      state.popupCallBack = response;
    },
    jumpTo(state, response) {
      state.jumpTo = response;
    },
    jump(state, response) {
      state.jump = response;
    },
    publicSite(state, response) {
      state.publicSite = response;
    }
  },

  actions: {
    load (context, data) {
      console.log("getData");
      context.commit('loading');
      //console.log(data);
      // pupil_in & weather will determine that stats are saved and which screen to show
      /*var tempData = {
        modes: [
          {id: 1, name: 'walk', class_name: 'walk', counter: 0},
          {id: 8, name: 'park & stride' ,class_name: 'park', counter: 0},
          {id: 2, name: 'bus', class_name: 'bus', counter: 0},
          {id: 3, name: 'bike' ,class_name: 'bike', counter: 0},
          {id: 4, name: 'car' ,class_name: 'car', counter: 0},
          {id: 5, name: 'train' ,class_name: 'train', counter: 0},
          {id: 6, name: 'scooter' ,class_name: 'scooter', counter: 0},
          {id: 7, name: 'share' ,class_name: 'share', counter: 0},
          {id: 9, name: 'taxi' ,class_name: 'taxi', counter: 0}
        ],
        counter: {
          1:0, 2:0, 3:0, 4:0, 5:0, 6:0, 7:0, 8:0, 9:0
        }
        pupil_total:2,//32,
        pupil_in:0,
        weather:0,
        points:0
      };*/
      /*var tempData = {
        classroom: {
          id: 1,
          name: "Classroom",
          description: "A test classroom",
          school_id: 1,
          admin_id: 1,
          region_id: 6,
          is_support: true,
          number_of_pupils: 6,
          pos:2//26
        },
        modes:{
          1:{name: "Walk", class_name: "walk"},
          2:{name: "Bus", class_name: "bus"},
          3:{name: "Bike", class_name: "bike"},
          4:{name: "Car", class_name: "car"},
          5:{name: "Train", class_name: "train"},
          6:{name: "Scooter", class_name: "scooter"},
          7:{name: "Share", class_name: "share"},
          8:{name: "Park & Stride", class_name: "park"},
          9:{name: "Taxi", class_name: "taxi"}
        },
        regions:{
          1:{id: 1, name:"All regions", schools:798},
          2:{id: 2, name:"N", schools:86},
          3:{id: 3, name:"County Durham", schools:96},
          4:{id: 4, name:"S", schools:16},
          5:{id: 5, name:"Gate", schools:6},
          6:{id: 6, name:"NT", schools:2},
          7:{id: 7, name:"NC", schools:15},
          8:{id: 8, name:"ST", schools:10}
        },
        leaderboard:{
          region_selected:6,
          name:"North Tyneside",
          table:{
            1:{
              1:{
                pos:1,
                name:"Beech Hill Primary",
                pupils:1234,
                classes:7,
                trips:347,
                most:1,
                points:257,
                modes: {
                  1:1, 2:5, 3:0, 4:6, 5:0, 6:6, 7:2, 8:0, 9:1
                }
              },
              2:{
                pos:2,
                name:"Washingwell Primary School",
                pupils:1234,
                classes:7,
                trips:347,
                most:2,
                points:257,
                modes: {
                  1:5, 2:6, 3:6, 4:0, 5:0, 6:0, 7:0, 8:0, 9:0
                }
              },
              3:{
                pos:3,
                name:"Washingwell Primary School",
                pupils:1234,
                classes:7,
                trips:347,
                most:1,
                points:257,
                modes: {
                  1:0, 2:0, 3:0, 4:0, 5:0, 6:0, 7:0, 8:0, 9:0
                }
              }
            },
            2:{
              11:{
                pos:11,
                name:"2 Beech Hill Primary",
                pupils:1234,
                classes:7,
                trips:347,
                most:1,
                points:257,
                modes: {
                  1:0, 2:0, 3:0, 4:0, 5:0, 6:0, 7:0, 8:0, 9:0
                }
              },
              12:{
                pos:12,
                name:"2 Washingwell Primary School",
                pupils:1234,
                classes:7,
                trips:347,
                most:1,
                points:257,
                modes: {
                  1:0, 2:0, 3:0, 4:0, 5:0, 6:0, 7:0, 8:0, 9:0
                }
              }
            },
            3:{
              26:{
                pos:26,
                name:"3 Beech Hill Primary",
                pupils:1234,
                classes:7,
                trips:347,
                most:1,
                points:257,
                modes: {
                  1:0, 2:0, 3:0, 4:0, 5:0, 6:0, 7:0, 8:0, 9:0
                }
              }
            }
          }
        }
      };*/
      // test
      if(data.child=="leaderboard"){
        /*if(data.data.local_authority==2){
          tempData.leaderboard.region_selected=2;
          tempData.leaderboard.name="Northumberland";
        }else if(data.data.local_authority!=1){
          tempData.leaderboard.region_selected=4;
          tempData.leaderboard.name="Sunderland";
          tempData.leaderboard.table={
            1:{
              1:{
                pos:1,
                name:"S Beech Hill Primary",
                pupils:1234,
                classes:7,
                trips:347,
                most:1,
                points:257,
                modes: {
                  1:0, 2:0, 3:0, 4:0, 5:0, 6:0, 7:0, 8:0, 9:0
                }
              },
              2:{
                pos:2,
                name:"S Washingwell Primary School",
                pupils:1234,
                classes:7,
                trips:347,
                most:2,
                points:257,
                modes: {
                  1:0, 2:0, 3:0, 4:0, 5:0, 6:0, 7:0, 8:0, 9:0
                }
              },
              3:{
                pos:3,
                name:"S Washingwell Primary School",
                pupils:1234,
                classes:7,
                trips:347,
                most:1,
                points:257,
                modes: {
                  1:0, 2:0, 3:0, 4:0, 5:0, 6:0, 7:0, 8:0, 9:0
                }
              },
              4:{
                pos:4,
                name:"S Washingwell Primary School",
                pupils:1234,
                classes:7,
                trips:347,
                most:1,
                points:257,
                modes: {
                  1:0, 2:0, 3:0, 4:0, 5:0, 6:0, 7:0, 8:0, 9:0
                }
              }
            },
            2:{
              11:{
                pos:11,
                name:"S2 Beech Hill Primary",
                pupils:1234,
                classes:7,
                trips:347,
                most:1,
                points:257,
                modes: {
                  1:0, 2:0, 3:0, 4:0, 5:0, 6:0, 7:0, 8:0, 9:0
                }
              },
              12:{
                pos:12,
                name:"S2 Washingwell Primary School",
                pupils:1234,
                classes:7,
                trips:347,
                most:1,
                points:257,
                modes: {
                  1:0, 2:0, 3:0, 4:0, 5:0, 6:0, 7:0, 8:0, 9:0
                }
              }
            }
          };

        }*/
        // context.commit('storeData', tempData);
        // return;
      } else if(data.url.indexOf('year')!=-1){
          var compareClassData = {
          today: "2016-11-17T00:00:00+0000",
          classroom: {
            id: 1,
            name: "Classroom",
            description: "A test classroom",
            school_id: 1,
            admin_id: 1,
            region_id: 6,
            is_support: true,
            number_of_pupils: 6,
            pos:2//26
          },
          modes:{
            1:{name: "Walk", class_name: "walk"},
            2:{name: "Bus", class_name: "bus"},
            3:{name: "Bike", class_name: "bike"},
            4:{name: "Car", class_name: "car"},
            5:{name: "Train", class_name: "train"},
            6:{name: "Scooter", class_name: "scooter"},
            7:{name: "Share", class_name: "share"},
            8:{name: "Park & Stride", class_name: "park"},
            9:{name: "Taxi", class_name: "taxi"}
          },
          regions:{
            0:{id: 0, name:"All regions", schools:798},
            1:{id: 1, name:"N", schools:86},
            2:{id: 2, name:"County Durham", schools:96},
            3:{id: 3, name:"S", schools:16},
            4:{id: 4, name:"Gate", schools:6},
            5:{id: 5, name:"NT", schools:2},
            6:{id: 6, name:"NC", schools:15},
            7:{id: 7, name:"ST", schools:10}
          },
          weather:{
            1:{id: 1, name: "Sunny", class_name: "sunny"},
            2:{id: 2, name: "Cloudy", class_name: "cloudy"},
            3:{id: 3, name: "Partly cloudy", class_name: "partly"},
            4:{id: 4, name: "Rainy", class_name: "rainy"},
            5:{id: 5, name: "Stormy", class_name: "stormy"},
            6:{id: 6, name: "Snowy", class_name: "snowy"},
            7:{id: 7, name: "Foggy", class_name: "foggy"},
            8:{id: 8, name: "Windy", class_name: "windy"}
          },

          //
          classDaysData:{
            today:{
              "2016-11-17":{
                date: "2016-11-17T00:00:00+0000",
                display_date:"Thursday 17th November",
                weather:3,
                modes:{
                  1:4, 2:1, 3:0, 4:2, 5:3, 6:0, 7:0, 8:0, 9:0
                }
              }
            },
            past:{
              "2016-11-16":{
                date: "2016-11-16T00:00:00+0000",
                display_date:"Wednesday 16th November",
                weather:1,
                modes:{
                  1:2, 2:0, 3:3, 4:4, 5:9, 6:0, 7:0, 8:0, 9:0
                }
              },
              "2016-11-17":{
                date: "2016-11-17T00:00:00+0000",
                display_date:"Thursday 17th November",
                weather:3,
                modes:{
                  1:0, 2:0, 3:0, 4:0, 5:0, 6:0, 7:0, 8:0, 9:0
                }
              },
              "2016-11-18":{
                date: "2016-11-18T00:00:00+0000",
                display_date:"Friday 18th November",
                weather:2,
                modes:{
                  1:0, 2:0, 3:0, 4:0, 5:0, 6:0, 7:0, 8:0, 9:0
                }
              }
            }
          },
          classWeeksData:{
            1:{
              date: "2016-11-14T00:00:00+0000",
              display_date:"Monday 14th",
              weather:1,
              pupils:100,
              most:1,
              least:2
            },
            2:{
              date: "2016-11-15T00:00:00+0000",
              display_date:"Tuesday 15th",
              weather:2,
              pupils:102,
              most:1,
              least:2
            },
            3:{
              date: "2016-11-16T00:00:00+0000",
              display_date:"Wednesday 16th",
              weather:1,
              pupils:98,
              most:1,
              least:2
            },
            4:{
              date: "2016-11-17T00:00:00+0000",
              display_date:"Thursday 17th",
              weather:1,
              pupils:98,
              most:1,
              least:2
            },
            5:{
              date: "2016-11-18T00:00:00+0000",
              display_date:"Friday 18th",
              weather:0,
              pupils:0,
              most:0,
              least:0
            }
          },

          //
          yearData:{
            local:{
              name:"Beech Hill Primary",
              number_of_pupils:1234,
              year:4,
              modes:{
                1:1, 2:0, 3:0, 4:5, 5:0, 6:0, 7:0, 8:0, 9:0
              }
            },
            regionData:{
              0:{
                name:"All Schools",
                number_of_pupils:1234,
                year:4,
                modes:{
                  1:3, 2:2, 3:0, 4:0, 5:0, 6:0, 7:0, 8:0, 9:0
                }
              },
              1:{
                name:"N",
                number_of_pupils:568,
                year:4,
                modes:{
                  1:5, 2:0, 3:4, 4:6, 5:0, 6:0, 7:0, 8:1, 9:2
                }
              },
              2:{
                name:"County Durham",
                number_of_pupils:1234,
                year:4,
                modes:{
                  1:0, 2:0, 3:0, 4:0, 5:0, 6:0, 7:0, 8:0, 9:0
                }
              },
              3:{
                name:"S",
                number_of_pupils:1234,
                year:4,
                modes:{
                  1:0, 2:0, 3:0, 4:0, 5:0, 6:0, 7:0, 8:0, 9:0
                }
              },
              4:{
                name:"Gate",
                number_of_pupils:1234,
                year:4,
                modes:{
                  1:0, 2:0, 3:0, 4:0, 5:0, 6:0, 7:0, 8:0, 9:0
                }
              },
              5:{
                name:"NT",
                number_of_pupils:1234,
                year:4,
                modes:{
                  1:0, 2:0, 3:0, 4:0, 5:0, 6:0, 7:0, 8:0, 9:0
                }
              },
              6:{
                name:"NC",
                number_of_pupils:1234,
                year:4,
                modes:{
                  1:0, 2:0, 3:0, 4:0, 5:0, 6:0, 7:0, 8:0, 9:0
                }
              },
              7:{
                name:"ST",
                number_of_pupils:1234,
                year:4,
                modes:{
                  1:0, 2:0, 3:0, 4:0, 5:0, 6:0, 7:0, 8:0, 9:0
                }
              }
            }
          }
        };
        context.commit('storeData', compareClassData);
        return;
      };
/*console.log("load success");
console.log(response.data);
      });*/

      //
      $.ajax({
        type: "GET",
        url: data.url,
        dataType: "json",
        data: data.data,
        cache: false,
        success: function(response){
          //console.log(response.data);
          if (response.success){
            console.log("load success");
            if(data.child=="compare"){
              //response.data.week_days=["2016-11-21", "2016-11-22", "2016-11-23", "2016-11-24", "2016-11-25"];
            }
            context.commit('storeData', response.data);
          } else {
            console.log("load success - error");
            var childData = {
              child:"state",
              data:{
                type:"alert",
                msg:"There was a Success - error.",
                buttons:[
                  {txt:"OK", val:null}
                ]
              }
            };
            context.dispatch('popup', childData);
          }
        },
        error: function(response){
          console.log("load error");
        }
      });
    },

    save (context, data) {
      //console.log("saveData");
      //console.log(data);
      $.ajax({
        type: "POST",
        url: data.url,
        data: data,
        dataType: "json",
        //data: data.data,
        cache: false,
        success: function(response){
          if (response.success){
            //console.log("save success");
            //response.data=tempData;
            context.commit('storeData', response.data);
            if(data.jumpTo){ context.dispatch('jumpTo', data.jumpTo); }
          } else {
            //console.log("save success - error");
            var childData = {
              child:"state",
              data:{
                type:"alert",
                msg:"There was a Success - error.",
                buttons:[
                  {txt:"OK", val:null}
                ]
              }
            };
            context.dispatch('popup', childData);
          }
        },
        error: function(response){
          console.log("save error");
        }
      });
    },

    popup (context, data) {
      //console.log("showPopup");
      context.commit('popupShow', true);
      context.commit('popupData', data.data);
    },

    popupClose (context, data) {
      //console.log("popupClose: ", data);
      context.commit('popupShow', false);
      if(data){ context.commit('popupCallBack', data); }
    },

    jumpTo (context, data) {
      //console.log("-- jumpTo: ", data);
      context.commit('jump', true);
      context.commit('jumpTo', data);
    },

    jumpComplete (context) {
      context.commit('jump', false);
    },

    removePopupCallBack (context) {
      context.commit('popupCallBack', null);
    },

    setPublicSite (context) {
      context.commit('publicSite', true);
    }

  },

  plugins: [statePersist]

});