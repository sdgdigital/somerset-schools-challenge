import * as types from '../mutation-types'


// initial state
const state = {
	user: {},
  started: false,
  finished: false,
  sessionId: 'NOT SET'
}

// getters
const getters = {

  getSess: state => {
    return state.sessionId
  },

	/*surveyStarted: function (state) {
		return state.started;
	}*/

}

// mutations
const mutations = {
  
  /*[types.START_SURVEY] (state, { args }) {
    state.started = true;
  },


  [types.START_SURVEY_FAILURE] (state, { args }) {
    state.started = true;
  },

  [types.START_SURVEY_SUCCESS] (state, { args }) {
    state.started = true;
  }*/

}


const actions = {
  
  /*startSurvey: function (request, callback, callbackErr) {
  	commit(types.START_SURVEY_SUCCESS);
  }*/
  /*shop.buyProducts(
    products,
    () => commit(types.CHECKOUT_SUCCESS),
    () => commit(types.CHECKOUT_FAILURE, { savedCartItems })
  )	*/
}


export default {
  state,
  getters,
  mutations,
  actions
}