
/**
 * First we will load all of this project's JavaScript dependencies.
 */

require('./bootstrap');

import Vue from 'vue'
import VueResource from 'vue-resource'
import VueRouter from 'vue-router'
import App from './App.vue'
import Store from './store'
import Home from './components/Home/Home.vue'
import Compare from './components/Compare/Compare.vue'
import Leaderboard from './components/Leaderboard/Leaderboard.vue'

/* Initialise the app to use Vue Resource */
Vue.use(VueResource)

/* Initialise the app to use Vue Router */
Vue.use(VueRouter)

// Create the router instance and defined `routes`, this is in history mode
const router = new VueRouter({ 
  mode: 'history', 
  routes: [ 
    { name: 'add-journeys', path: '/add-journeys', component: Home },
    { name: 'compare', path: '/compare', component: Compare },
    { name: 'leaderboard', path: '/leaderboard', component: Leaderboard },
    { name: 'leaderboard-public', path: '/leaderboard-public', component: Leaderboard }
  ]
})

/* Initialise the main Vue App */
const app = new Vue({
	name: 'VueRootApp',
	router:router,
  store:Store, // inject the store instance to all child components.
	template: '<App/>',
	components: { App }
  // data: {
  //   savedData: null
  // },
  // created: function () {
  //   this.fetchData();
  // },
  // methods: {
  //   fetchData: function () {
  //     var self = this;
  //     //fetchedData = e + 'Data';      
  //     $.get( 'js/saved.json', function( data ) {
  //        self.savedData = data;
  //     });
  //   }   
  // }    
}).$mount('#app');
