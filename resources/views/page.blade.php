@extends('layouts.custom-page')

@section('title', $model->name)

@section('content')

    {!! $model->html !!}

@endsection