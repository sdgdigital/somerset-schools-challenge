Hi {{ $user['display_name'] }},<br>
<br>
Your account for the {{ config('app.name') }} has now been created.<br>
You can now register your class and then log your pupils journeys to school on the agreed survey day.<br>
<br>
You can log in to the site <a href="{{ route('login') }}">here</a>.
<br>
<br>
Happy and sustainable travelling!<br>
<br>
From The Bridgwater Way team<br>
<br>