<div class="masthead">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1>Masthead</h1>
                <ul>
                    <li>Logged in as {{ $user->full_name }}</li>
                    <li><a href="{{ route('my-account.edit') }}">My Account</a></li>
                    <li>@include('partials.logout')</li>
                </ul>
                <hr>
            </div>
        </div>
    </div>
</div>
