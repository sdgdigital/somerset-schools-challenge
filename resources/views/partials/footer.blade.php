<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <span><span class="fa fa-copyright fa-fw"></span> {{ $today->format('Y') }} Somerset County Council</span>
                <span> | </span>
                <a href="{{ route('terms-and-conditions') }}">Terms and conditions</a>
            </div>
        </div>
    </div>
</footer>