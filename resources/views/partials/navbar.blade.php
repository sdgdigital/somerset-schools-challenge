<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ route('dashboard') }}">{{ config('app.name') }}</a>
        </div>
        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                @if (Auth::guest())
                    <li><a href="{{ route('login') }}">{{ ucfirst(trans('app.login')) }}</a></li>
                    <li><a href="{{ route('register') }}">{{ ucfirst(trans('app.register')) }}</a></li>
                @else
                    <li><a href="{{ route('dashboard') }}">Dashboard</a></li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> Manage <span class="fa fa-caret-down"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            @can('super')
                                <li><a href="{{ route('options.index') }}"><span class="fa fa-cogs fa-fw"></span> Options</a></li>
                                <li><a href="{{ route('competitions.index') }}"><span class="{{ trans('app.competitions-icon') }}"></span> {{ ucfirst(trans('app.competitions')) }}</a></li>
                                <li><a href="{{ route('modes.index') }}"><span class="{{ trans('app.modes-icon') }}"></span> {{ ucfirst(trans('app.modes')) }}</a></li>
                                <li><a href="{{ route('pages.index') }}"><span class="{{ trans('app.pages-icon') }}"></span> {{ ucfirst(trans('app.pages')) }}</a></li>
                                <li><a href="{{ route('reports.index') }}"><span class="{{ trans('app.reports-icon') }}"></span> {{ ucfirst(trans('app.reports')) }}</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="{{ route('local-authorities.index') }}"><span class="{{ trans('app.local-authorities-icon') }}"></span> {{ ucfirst(trans('app.local-authorities')) }}</a></li>
                                <li><a href="{{ route('schools.index') }}"><span class="{{ trans('app.schools-icon') }}"></span> {{ ucfirst(trans('app.schools')) }}</a></li>
                            @endcan
                            @can('admin')
                                <li><a href="{{ route('classrooms.index') }}"><span class="{{ trans('app.classrooms-icon') }}"></span> {{ ucfirst(trans('app.classrooms')) }}</a></li>
                            @endcan
                            @can('super')
                                <li><a href="{{ route('users.index') }}"><span class="{{ trans('app.users-icon') }}"></span> {{ ucfirst(trans('app.users')) }}</a></li>
                            @endcan
                            @can('teach')
                             @if(!empty($user->classroom))
                                <li><a href="{{ route('trips.index') }}"><span class="{{ trans('app.trips-icon') }}"></span> {{ ucfirst(trans('app.trips')) }}</a></li>
                             @endif
                            @endcan
                        </ul>
                    </li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> Hi {{ $user->primary_name }} <span class="fa fa-caret-down"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ route('my-account.edit') }}">My account</a></li>
                        </ul>
                    </li>
                    <li>
                        @include('partials.logout')
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>