<script>
var initCheck=true;
document.onreadystatechange = function(){
	if(initCheck){ 
		initCheck=false;
		if(getIEVersion()){ 
			if(document.getElementsByClassName("panel-login").length){ // login
				document.getElementsByClassName("panel-login")[0].className+=" hide";
				document.getElementsByClassName("panel-warning")[0].className="panel-warning";
			}else{ // dashboard
				document.getElementById("dashboard-menu").className+=" hide";
				document.getElementById("dashboard-warning").className="row";
			}
		}
	}
}

function getIEVersion() {
	var sAgent = window.navigator.userAgent;
	var Idx = sAgent.indexOf("MSIE");

	// If IE, return version number.
	if (Idx > 0) 
	  return parseInt(sAgent.substring(Idx+ 5, sAgent.indexOf(".", Idx)));

	// If IE 11 then look for Updated user agent string.
	else if (!!navigator.userAgent.match(/Trident\/7\./)) 
	  //return 11;
	  return false;

	else
	  //return 0; //It is not IE
	  return false;
}
</script>