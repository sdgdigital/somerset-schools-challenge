@extends('layouts.app')

@section('content')

    <?php include_once("img/svg/sprite.svg"); ?>
    <div id="app"></div>
    <script src="{!! elixir('js/app.js') !!}"></script>

@endsection

