@extends('layouts.admin')

@section('title', 'Create ' . trans('app.' . str_singular($resource)))

@section('content')

    @include('flash::message')
    @include('admin.components.header')
    @include('admin.partials.' . $resource . '-create')

@endsection