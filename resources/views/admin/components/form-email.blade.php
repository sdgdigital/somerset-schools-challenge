<div class="form-group{{ $errors->has($name) ? ' has-error' : '' }}">
    {{ Form::bsLabel($name, $label) }}
    <div class="input-group">
        <span class="input-group-addon"><span class="fa fa-envelope-o fa-fw"></span></span>
        {{ Form::email($name, $value, mergeCssAttributes($attributes, 'form-control')) }}
    </div>
    @include('admin.components.form-help-block')
</div>
