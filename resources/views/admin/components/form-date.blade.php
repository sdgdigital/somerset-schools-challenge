<div class="form-group{{ $errors->has($name) ? ' has-error' : '' }}">
    {{ Form::bsLabel($name, $label) }}
    <div class="input-group date datepicker" data-provide="datepicker">
        <span class="input-group-addon"><span class="fa fa-calendar fa-fw"></span></span>
        {{ Form::text($name, $value, mergeCssAttributes($attributes, 'form-control')) }}
    </div>
    @include('admin.components.form-help-block')
</div>
