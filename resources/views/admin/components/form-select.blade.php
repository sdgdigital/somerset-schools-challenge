<div class="form-group{{ $errors->has($name) ? ' has-error' : '' }}">
    {{ Form::bsLabel($name, $label) }}
    {{ Form::select($name, $list, $value, mergeCssAttributes($attributes, 'form-control')) }}
    @include('admin.components.form-help-block')
</div>
