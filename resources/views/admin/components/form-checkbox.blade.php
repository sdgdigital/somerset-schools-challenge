<div class="checkbox{{ $errors->has($name) ? ' has-error' : '' }}">
    {{ Form::hidden($name, 0) }}
    <label>
        {{ Form::checkbox($name, 1, $checked) }} {!! $label !!}
    </label>
    @if($errors->has($name))
        <span class="help-block">{{ $errors->first($name) }}</span>
    @endif
</div>
