<div class="form-group{{ $errors->has($name) ? ' has-error' : '' }}">
    {{ Form::label($name, $label) }}
    {{ Form::file($name) }}
    @include('admin.components.form-help-block')
</div>
