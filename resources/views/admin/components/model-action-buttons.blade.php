<div class="btn-group btn-group-xs pull-right">
    @if(\Route::has($resource . '.edit'))
        <a class="btn btn-xs btn-default js-btn-edit" href="{{ route($resource . '.edit', $model->id) }}">Edit</a>
    @endif
        @if(\Route::has($resource . '.destroy'))
        <a class="btn btn-xs btn-danger js-btn-delete" href="{{ route($resource . '.destroy', $model->id) }}" onclick="return confirm('Are you sure you want to delete this {{ trans('app.' . str_singular($resource)) }}?') ? true : false ;" data-model-id="{{ $model->id }}">Delete</a>
    @endif
</div>
@if(\Route::has($resource . '.destroy'))
    <form id="delete-{{ $model->id }}" style="display: none;" method="post" action="{{ route($resource . '.destroy', $model->id) }}">
        {{ csrf_field() }}
        {{ method_field('DELETE') }}
    </form>
@endif