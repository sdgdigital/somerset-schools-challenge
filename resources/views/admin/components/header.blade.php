<?php
switch (getResourceAction()) {
    case 'create':
        $str = 'Create ' . trans('app.' . str_singular($resource));
        break;
    case 'edit':
        $str = 'Edit ' . trans('app.' . str_singular($resource));
        break;
    default:
        $str = 'Manage ' . trans('app.' . $resource);
        break;
}
?>
<div class="page-header">
    <h1><span class="{{ trans('app.' . $resource . '-icon') }}"></span> {{ $str }}</h1>
</div>
