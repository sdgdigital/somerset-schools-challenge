<div class="form-group">
    @if(getResourceAction() == 'edit')
        {{ Form::bsButtonUpdate() }}
    @else
        {{ Form::bsButtonCreate() }}
    @endif
    <a href="{{ route($resource . '.index') }}" class="btn btn-link">Cancel</a>
</div>
