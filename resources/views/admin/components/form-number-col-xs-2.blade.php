<div class="form-group col-xs-2{{ $errors->has($name) ? ' has-error' : '' }}">
    {{ Form::bsLabel($name, $label) }}
    {{ Form::number($name, $value, mergeCssAttributes($attributes, 'form-control')) }}
    @include('admin.components.form-help-block')
</div>
