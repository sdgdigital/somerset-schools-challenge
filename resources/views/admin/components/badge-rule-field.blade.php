<div class="form-group{{ $errors->has('rules[' . $key . '][value]') ? ' has-error' : '' }}">
    {{ Form::hidden('rules[' . $key . '][type]', $rule->type) }}
    @if ($rule->type == 'date')
        {{ Form::label('rules[' . $key . '][value]', 'Date') }}
        <div class="input-group date" data-provide="datepicker">
            <span class="input-group-addon"><span class="fa fa-calendar fa-fw"></span></span>
            {{ Form::text('rules[' . $key . '][value]', $rule->date->format($options->date_format), ['class' => 'form-control', 'placeholder' => $options->date_format_js]) }}
            <div class="input-group-btn">
                <button type="button" class="btn btn-default js-rule-delete"><span class="fa fa-trash fa-fw" aria-hidden="true"></span><span class="sr-only">Delete</span></button>
            </div>
        </div>
    @endif
    @if ($rule->type == 'point')
        {{ Form::label('rules[' . $key . '][value]', 'Point') }}
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-chevron-right fa-fw"></span></span>
            {{ Form::text('rules[' . $key . '][value]', $rule->point, ['class' => 'form-control', 'placeholder' => 'e.g. 20.5']) }}
            <div class="input-group-btn">
                <button type="button" class="btn btn-default js-rule-delete"><span class="fa fa-trash fa-fw" aria-hidden="true"></span><span class="sr-only">Delete</span></button>
            </div>
        </div>
    @endif
    @if ($rule->type == 'mode')
        {{ Form::label('rules[' . $key . '][value]', 'Mode') }}
        <div class="input-group">
            {{ Form::select('rules[' . $key . '][value]', $modeList, $rule->mode, ['class' => 'form-control']) }}
            <div class="input-group-btn">
                <button type="button" class="btn btn-default js-rule-delete"><span class="fa fa-trash fa-fw" aria-hidden="true"></span><span class="sr-only">Delete</span></button>
            </div>
        </div>
    @endif
    {{ $errors->first('rules[' . $rule->id . '][value]', '<p class="help-block">:message</p>') }}
</div>
