@if(\Route::has($resource . '.create'))
    <div class="well well-sm">
        <a class="btn btn-default btn-sm" href="{{ route($resource . '.create') }}"><span class="fa fa-plus fa-fw" aria-hidden="true"></span> Add a new {{ trans('app.' . str_singular($resource)) }}</a>
    </div>
@endif
