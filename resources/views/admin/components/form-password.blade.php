<div class="form-group{{ $errors->has($name) ? ' has-error' : '' }}">
    {{ Form::bsLabel($name, $label) }}
    <div class="input-group">
        <span class="input-group-addon"><span class="fa fa-key fa-fw"></span></span>
        {{ Form::password($name, mergeCssAttributes($attributes, 'form-control')) }}
    </div>
    @include('admin.components.form-help-block')
</div>
