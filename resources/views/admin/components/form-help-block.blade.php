<span class="help-block">
    @if($errors->has($name))
        <strong>{{ $errors->first($name) }} </strong>
    @endif
    @if(!empty($attributes['help']))
        {!! $attributes['help'] !!}
    @endif
</span>
