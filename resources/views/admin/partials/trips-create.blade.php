{{ Form::open(['route' => $resource . '.store']) }}
@can('super')
    {{ Form::bsSelect('classroom_id', 'Classroom', old('classroom_id'), $classroomList) }}
@else
    {{ Form::hidden('classroom_id', auth()->user()->classroom->id) }}
@endcan
{{ Form::bsDate('date', 'Date', old('date')) }}
{{ Form::bsSelect('weather_id', 'Weather', old('weather_id'), $weatherList) }}
<div class="panel panel-default{{ $errors->has('total') ? ' panel-danger has-error' : '' }}">
    <div class="panel-heading">
        <h1 class="panel-title">{{ ucfirst(trans('app.trip')) }} {{ trans('app.modes') }}</h1>
    </div>
    <div class="panel-body">
        <div class="row">
            {{ Form::bsNumberCustom('walk', 'Walk'), old('walk') }}
            {{ Form::bsNumberCustom('bike', 'Bike'), old('bike') }}
            {{ Form::bsNumberCustom('scooter', 'Scooter'), old('scooter') }}
            {{ Form::bsNumberCustom('bus', 'Bus'), old('bus') }}
            {{ Form::bsNumberCustom('train', 'Train'), old('train') }}
            {{ Form::bsNumberCustom('share', 'Share'), old('share') }}
            {{ Form::bsNumberCustom('park', 'Park & Stride'), old('park') }}
            {{ Form::bsNumberCustom('car', 'Car'), old('car') }}
            {{ Form::bsNumberCustom('taxi', 'Taxi'), old('taxi') }}
            {{ Form::bsNumberCustom('wheelchair', 'Wheelchair'), old('wheelchair') }}
        </div>
    </div>
</div>
{{ Form::bsSubmit() }}
{{ Form::close() }}
