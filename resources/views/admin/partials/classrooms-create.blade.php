{{ Form::open(['route' => $resource . '.store']) }}
{{ Form::hidden('school_id', app('user')->school_id) }}
@can('super')
{{ Form::bsSelect('teacher_id', ucfirst(trans('app.teacher')), (empty(old('teacher_id')) ? null : old('teacher_id')), $userList, ['help' => '<a href="' . route('users.create') . '">Add a new ' . trans('app.user') . '</a>']) }}
@elsecan('admin')
{{ Form::hidden('teacher_id', app('user')->id) }}
@endcan
{{ Form::bsText('name', ucfirst(trans('app.classroom')) . ' name', old('name')) }}
{{ Form::bsSelect('year_group', 'Year group', (empty(old('year_group')) ? null : old('year_group')), $yearGroupList) }}
{{ Form::bsNumber('number_of_pupils', 'Number of pupils', old('number_of_pupils')) }}
{{ Form::bsSubmit() }}
{{ Form::close() }}
