@include('admin.components.model-add-button')
@if($collection->count())
    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
            <tr>
                @can('super')
                    <th>ID</th>
                @endcan
                <th>Date</th>
                @can('super')
                    <th>Classroom</th>
                @endcan
                <th>Trips</th>
                <th>Points</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach ($collection as $model)
                <tr>
                    @can('super')
                        <td>{{ $model->id }}</td>
                    @endcan
                    <td>{{ $model->date->format(app('options')->date_format) }}</td>
                    @can('super')
                        <th>{{ $model->classroom->name }}</th>
                    @endcan
                    <td>{{ $model->total_trips }}/{{ $model->classroom->number_of_pupils }}</td>
                    <td>{{ $model->total_points }}</td>
                    <td>
                        @include('admin.components.model-action-buttons', ['id' => $model->id])
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endif