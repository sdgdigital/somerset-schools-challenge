{{ Form::model($model, ['route' => [$resource . '.update', $model->id]]) }}
{{ method_field('PATCH') }}
{{ Form::bsText('name', 'Name', $model->name) }}
{{ Form::bsTextArea('description', 'Description', $model->description) }}
{{ Form::bsDate('start_date', 'Start date', $model->start_date->format($options->date_format), ['help' => 'The date the challenge starts and the first day trips start counting toward the challenge.']) }}
{{ Form::bsDate('end_date', 'End date', $model->end_date->format($options->date_format), ['help' => 'The last official day of the challenge and the last day trips will count toward the challenge.']) }}
{{ Form::bsDate('grace_period', 'Grace period', $model->grace_period->format($options->date_format), ['help' => 'The period after a challenge that trips can still be logged for the challenge.']) }}
{{ Form::bsNumber('duration', 'Duration', $model->duration, ['help' => 'The number of active days in the competition. Excluding public holidays and weekends.']) }}
{{ Form::bsCheckbox('is_active', 'Enable ' . trans('app.competition'), $model->is_active) }}
{{ Form::bsSubmit() }}
{{ Form::close() }}
