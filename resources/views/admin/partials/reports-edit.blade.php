<a target="_blank" class="btn btn-default" href="{{ route('reports.users') }}"><span class="fa fa-download fa-fw"></span> Download {{ trans('app.users') }} report</a><br><br>
<a target="_blank" class="btn btn-default" href="{{ route('reports.schools') }}"><span class="fa fa-download fa-fw"></span> Download {{ trans('app.schools') }} report</a><br><br>
<a target="_blank" class="btn btn-default" href="{{ route('reports.trips') }}"><span class="fa fa-download fa-fw"></span> Download {{ trans('app.trips') }} report</a><br><br>
<a target="_blank" class="btn btn-default" href="{{ route('reports.year-group') }}"><span class="fa fa-download fa-fw"></span> Download year group report</a>
