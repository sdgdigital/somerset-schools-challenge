{{ Form::model($model, ['route' => [$resource . '.update', $model->id], 'files' => true]) }}
{{ method_field('PATCH') }}
{{ Form::bsText('name', 'Name', $model->name) }}
{{ Form::bsCheckbox('is_support', 'Is support?', $model->is_support) }}
{{ Form::bsSubmit() }}
{{ Form::close() }}
