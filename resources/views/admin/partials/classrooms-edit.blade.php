{{ Form::model($model, ['route' => [$resource . '.update', $model->id], 'files' => true]) }}
{{ method_field('PATCH') }}
{{ Form::hidden('school_id', $model->school_id) }}
@can('super')
{{ Form::bsSelect('teacher_id', ucfirst(trans('app.teacher')), (empty($model->teacher) ? null : $model->teacher->id), $userList) }}
@elsecan('admin')
{{ Form::hidden('teacher_id', $model->teacher->id) }}
@endcan
{{ Form::bsText('name', ucfirst(trans('app.classroom')) . ' name', $model->name) }}
{{ Form::bsSelect('year_group', 'Year group', (empty($model->year_group) ? null : $model->year_group), $yearGroupList) }}
{{ Form::bsNumber('number_of_pupils', 'Number of pupils', $model->number_of_pupils) }}
{{ Form::bsSubmit() }}
{{ Form::close() }}
