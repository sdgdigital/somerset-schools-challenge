@include('admin.components.model-add-button')
@if($collection->count())
<div class="table-responsive">
    <table class="table table-hover">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Points</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($collection as $model)
            <tr>
                <td>{{ $model->id }}</td>
                <td>{{ $model->name }}</td>
                <td>{{ $model->points }}</td>
                <td>
                    @include('admin.components.model-action-buttons')
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@endif