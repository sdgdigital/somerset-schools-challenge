@include('admin.components.model-add-button')
@if($collection->count())
    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                @can('super')
                    <th>{{ ucfirst(trans('app.school')) }}</th>
                @endcan
                <th>{{ ucfirst(trans('app.classroom')) }} name</th>
                <th>Assigned</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach ($collection as $model)
                <tr>
                    <td>{{ $model->id or '' }}</td>
                    <td>{{ $model->display_name or '' }}</td>
                    <td>{{ $model->email or '' }}</td>
                    @can('super')
                        <td>
                            @if(empty($model->school))
                                -
                            @else
                                {{ $model->school->name or '-' }}
                            @endif
                        </td>
                    @endcan
                    <td>
                        @if(empty($model->classroom))
                            -
                        @else
                            {{ $model->classroom->name or '-' }}
                        @endif
                    </td>
                    <td>
                        @if(empty($model->classroom->pivot->updated_at))
                            -
                        @else
                            {{ $model->classroom->pivot->updated_at->format($options->date_format) }}
                        @endif
                    </td>
                    <td>
                        @include('admin.components.model-action-buttons', ['id' => $model->id])
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endif