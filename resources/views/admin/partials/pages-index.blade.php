@if($collection->count())
<div class="table-responsive">
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Name</th>
            <th>URL</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($collection as $model)
            <tr>
                <td>{{ $model->name }}</td>
                <td><a href="{{ url('/') }}{{ $model->url }}" target="_blank">{{ $model->url }} <span class="fa fa-external-link fa-fw"></span></a></td>
                <td>
                    <form action="" class="text-right">
                        <a href="{{ route($resource . '.edit', $model->id) }}" class="btn btn-xs btn-default">Edit</a>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@endif