{{ Form::model($model, ['route' => [$resource . '.update', $model->id]]) }}
{{ method_field('PATCH') }}
@can('super')
    {{ Form::bsSelect('classroom_id', 'Classroom', $model->classroom_id, $classroomList) }}
@else
    {{ Form::hidden('classroom_id', $model->classroom_id) }}
@endcan
{{ Form::bsDate('date', 'Date', $model->date->format($options->date_format)) }}
{{ Form::bsSelect('weather_id', 'Weather', $model->weather_id, $weatherList) }}
<div class="panel panel-default{{ $errors->has('total') ? ' panel-danger has-error' : '' }}">
    <div class="panel-heading">
        <h1 class="panel-title">{{ ucfirst(trans('app.trip')) }} {{ trans('app.modes') }}</h1>
    </div>
    <div class="panel-body">
        <div class="row">
            {{ Form::bsNumberCustom('walk', 'Walk', $model->walk) }}
            {{ Form::bsNumberCustom('bike', 'Bike', $model->bike) }}
            {{ Form::bsNumberCustom('scooter', 'Scooter', $model->scooter) }}
            {{ Form::bsNumberCustom('bus', 'Bus', $model->bus) }}
            {{ Form::bsNumberCustom('train', 'Train', $model->train) }}
            {{ Form::bsNumberCustom('share', 'Share', $model->share) }}
            {{ Form::bsNumberCustom('park', 'Park & Stride', $model->park) }}
            {{ Form::bsNumberCustom('car', 'Car', $model->car) }}
            {{ Form::bsNumberCustom('taxi', 'Taxi', $model->taxi) }}
            {{ Form::bsNumberCustom('wheelchair', 'Wheelchair', $model->wheelchair) }}
        </div>
    </div>
</div>
{{ Form::bsSubmit() }}
{{ Form::close() }}
