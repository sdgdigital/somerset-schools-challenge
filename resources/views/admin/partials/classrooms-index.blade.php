@if(!$assigned)
    @include('admin.components.model-add-button')
@endif
@if($collection->count())
<div class="table-responsive">
    <table class="table table-hover">
        <thead>
        <tr>
            <th>@can('super') ID @endcan</th>
            <th>{{ ucfirst(trans('app.classroom')) }} name</th>
            <th>Year group</th>
            <th>{{ ucfirst(trans('app.teacher')) }} name</th>
            <th>School</th>
            <th>Assigned</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach ($collection as $model)
            <tr>
                <td>@can('super') {{ $model->id or '-' }} @endcan</td>
                <td>{{ $model->name or '-' }}</td>
                <td>{{ $model->year_group or '-' }}</td>
                <td>{{ $model->teacher->full_name or '-' }}</td>
                <td>{{ $model->teacher->school->name or '-' }}</td>
                <td>
                    @if(empty($model->teacher->pivot->updated_at))
                        -
                    @else
                        {{ $model->teacher->pivot->updated_at->format($options->date_format) }}
                    @endif
                </td>
                <td>
                @if(auth()->user()->id === $model['teacher']['id'])
                    @include('admin.components.model-action-buttons', ['edit' => true, 'delete' => true])
                @else
                    @can('super')
                        @include('admin.components.model-action-buttons', ['edit' => true, 'delete' => true])
                    @endcan
                @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@endif