{{ Form::model($model, ['route' => ['users.update', $model->id]]) }}
{{ method_field('PATCH') }}
@can('super')
    {{ Form::bsSelect('school_id', 'School', $model->school_id, $schoolList) }}
@endcan
{{ Form::bsText('email', 'Email', $model->email) }}
{{ Form::bsText('display_name', 'Display name', $model->display_name) }}
{{ Form::bsText('first_name', 'First name', $model->first_name) }}
{{ Form::bsText('last_name', 'Last name', $model->last_name) }}
{{ Form::bsSubmit() }}
{{ Form::close() }}
