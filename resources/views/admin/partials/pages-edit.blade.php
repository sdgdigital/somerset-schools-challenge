@push('additional-header')
<link rel="stylesheet" href="//cdn.jsdelivr.net/simplemde/latest/simplemde.min.css">
<script src="//cdn.jsdelivr.net/simplemde/latest/simplemde.min.js"></script>
@endpush

{{ Form::model($model, ['route' => [$resource . '.update', $model->id]]) }}
{{ method_field('PATCH') }}
{{ Form::bsText('name', 'Title', $model->name) }}
{{ Form::bsText('url', 'URL', $model->url, ['readonly']) }}
{{ Form::bsTextArea('body', 'Body', $model->body, ['help' => 'Edit the page using <a target="_blank" href="https://daringfireball.net/projects/markdown/basics">markdown syntax</a>']) }}
<script>
    // instantiate the markdown editor
    var simplemde = new SimpleMDE({element: $("#body")[0]});
</script>
{{ Form::bsSubmit() }}
{{ Form::close() }}
