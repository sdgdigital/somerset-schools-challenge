{{ Form::model($model, ['route' => [$resource . '.update']]) }}
{{ Form::bsSelect('distance_unit_abbr', 'Distance units', $model->distance_unit_abbr, $model->distanceUnitList) }}
{{ Form::bsSelect('weight_unit_abbr', 'Weight units', $model->weight_unit_abbr, $model->weightUnitList) }}
<hr>
{{ Form::bsSelect('date_format', 'Date format', $model->date_format, $model->dateFormatList) }}
{{ Form::bsText('date_format_pretty', 'Date format (long)', $model->date_format_pretty, ['placeholder' => 'e.g. j F, \a\t h:i', 'help' => '<a href="http://php.net/manual/en/function.date.php">PHP date format reference</a>']) }}
{{ Form::bsText('date_format_short', 'Date format (short)', $model->date_format_short, ['placeholder' => 'e.g. j F', 'help' => '<a href="http://php.net/manual/en/function.date.php">PHP date formats reference</a>']) }}
<hr>
{{ Form::bsText('google_code', 'Google analytics code', $model->google_code, ['placeholder' => 'e.g. UA-XXXXXXX-X']) }}
{{ Form::bsSubmit() }}
{{ Form::close() }}
