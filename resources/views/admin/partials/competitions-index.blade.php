@include('admin.components.model-add-button')
@if($collection->count())
<div class="table-responsive">
    <table class="table table-hover">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Start date</th>
            <th>End date</th>
            <th>Grace period</th>
            <th>Active</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($collection as $model)
            <tr>
                <td>{{ $model->id }}</td>
                <td>{{ $model->name }}</td>
                <td>{{ $model->start_date->format($options->date_format) }}</td>
                <td>{{ $model->end_date->format($options->date_format) }}</td>
                <td>{{ $model->grace_period->format($options->date_format) }}</td>
                <td>{{ $model->is_active ? 'Yes' : 'No' }}</td>
                <td class="text-right">
                    @include('admin.components.model-action-buttons')
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@endif