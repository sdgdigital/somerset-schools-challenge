{{ Form::model($model, ['route' => [$resource . '.update', $model->id]]) }}
{{ method_field('PATCH') }}
{{ Form::bsText('name', 'Name', $model->name, ['readonly']) }}
{{ Form::bsText('class_name', 'CSS class name', $model->class_name, ['readonly']) }}
{{ Form::bsNumber('points', 'Points', $model->points) }}
{{ Form::bsSubmit() }}
{{ Form::close() }}
