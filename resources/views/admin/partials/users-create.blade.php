{{ Form::open(['route' => $resource . '.store']) }}
@can('super')
    {{ Form::bsSelect('school_id', 'School', old('school_id'), $schoolList) }}
@endcan
{{ Form::bsText('first_name', 'First name', old('first_name')) }}
{{ Form::bsText('last_name', 'Last name', old('last_name')) }}
{{ Form::bsText('display_name', 'Display name', old('display_name')) }}
{{ Form::bsText('email', 'Email', old('email')) }}
{{ Form::bsSubmit() }}
{{ Form::close() }}
