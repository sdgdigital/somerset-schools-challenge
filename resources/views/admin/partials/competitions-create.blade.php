{{ Form::open(['route' => $resource . '.store']) }}
{{ Form::bsText('name', 'Name', old('name')) }}
{{ Form::bsTextArea('description', 'Description', old('description')) }}
{{ Form::bsDate('start_date', 'Start date', old('start_date'), ['help' => 'The date the challenge starts and the first day trips start counting toward the challenge.']) }}
{{ Form::bsDate('end_date', 'End date', old('end_date'), ['help' => 'The last official day of the challenge and the last day trips will count toward the challenge.']) }}
{{ Form::bsDate('grace_period', 'Grace period', old('grace_period'), ['help' => 'The period after a challenge that trips can still be logged for the challenge.']) }}
{{ Form::bsNumber('duration', 'Duration', old('duration'), ['help' => 'The number of active days in the competition. Excluding public holidays and weekends.']) }}
{{ Form::bsCheckbox('is_active', 'Enable ' . trans('app.competition'), old('is_active')) }}
{{ Form::bsSubmit() }}
{{ Form::close() }}
