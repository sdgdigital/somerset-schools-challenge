@extends('layouts.admin')

@section('title', 'Manage ' . trans('app.' . $resource))

@section('content')

    @include('flash::message')
    @include('admin.components.header')
    @include('admin.partials.' . $resource . '-index')
    @include('admin.components.model-pagination')

@endsection