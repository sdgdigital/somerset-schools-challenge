@extends('layouts.admin')

@section('title', 'Teacher')

@section('content')

    @include('flash::message')
    <div class="jumbotron">
        @if($morning)
            <h1>Good morning</h1>
        @else
            <h1>Good afternoon</h1>
        @endif

        <!-- <p>{{ ucfirst(trans('app.competition')) }} runs from {{ app('competition')->start_date->format(app('options')->date_format_short) }} to {{ app('competition')->end_date->format(app('options')->date_format_short) }}</p> -->
        <div id="dashboard-menu" class="row">
            @if(!empty($user->classroom))
                <div class="col-xs-3">
                    <a class="c-btn c-btn--green" href="{{ route('add-journeys') }}">
                        <span class="c-btn__content"><span class="fa fa-fw fa-plus"></span>Add Journeys</span>
                    </a>
                </div>
                <div class="col-xs-3">
                    <a class="c-btn c-btn--green" href="{{ route('compare') }}">
                        <span class="c-btn__content"><span class="fa fa-fw fa-plus"></span>Compare</span>
                    </a>
                </div>
            @endif
            @if(auth()->user()->can('super') || !empty($user->classroom))
                <div class="col-xs-3">
                    <a class="c-btn c-btn--green" href="{{ route('leaderboard') }}">
                        <span class="c-btn__content"><span class="fa fa-fw fa-plus"></span>Leaderboard</span>
                    </a>
                </div>
            @endif
            @can('super')
                <div class="col-xs-3">
                    <a class="c-btn c-btn--green" href="{{ route('users.index') }}"><span class="c-btn__content"><span class="fa fa-fw fa-plus"></span>Add Teachers</span></a>
                </div>
            @endcan
            @can('admin')
                <div class="col-xs-3">
                    <a class="c-btn c-btn--green" href="{{ route('classrooms.index') }}"><span class="c-btn__content"><span class="fa fa-fw fa-plus"></span>Add {{ ucfirst(trans('app.classrooms')) }}</span></a>
                </div>
            @endcan

            <div class="col-xs-3">
                <a class="c-btn c-btn--green" href="{{ route('how-it-works') }}">
                    <span class="c-btn__content">How it works</span>
                </a>
            </div>

        </div>

        <div id="dashboard-warning" class="row hide">
            <div class="panel panel-default">
                <div class="panel-warning">
                    <div class="panel-heading">
                        <h1 class="panel-title">Update your browser!</h1>
                    </div>
                    <div class="panel-body">
                        The Somerset Schools Challenge website has been designed to work with modern web browsers. We have detected that you are not using one of our recommended browsers and would strongly suggest you ask your IT department to upgrade you.<br/><br/>Please use one of the links below for more information:<br/><br/>
                        <div class="form-group">
                            <a class="btn btn-link btn-block" href='http://www.apple.com/safari' target='_blank'>Apple Safari</a>
                            <a class="btn btn-link btn-block" href='https://www.google.com/chrome' target='_blank'>Google Chrome</a>
                            <a class="btn btn-link btn-block" href='http://windows.microsoft.com/en-us/internet-explorer/download-ie' target='_blank'>Microsoft Internet Explorer</a>
                            <a class="btn btn-link btn-block" href='http://www.browserfordoing.com' target='_blank'>Microsoft Edge</a>
                            <a class="btn btn-link btn-block" href='https://www.mozilla.org/en-US/firefox/new' target='_blank'>Mozilla Firefox</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection
