@extends('layouts.admin')

@section('title', 'My Account')

@section('content')

    @include('flash::message')
    <div class="page-header">
        <h1>My Account</h1>
    </div>
    {{ Form::model($model, ['route' => ['my-account.update'], 'files' => true]) }}
    {{ Form::bsText('email', 'Email',  $model->email, ['required']) }}
    {{ Form::bsText('display_name', 'Display name <small>(optional)</small>', $model->display_name, ['placeholder' => 'What shall we call you?']) }}
    {{ Form::bsText('first_name', 'First name <small>(optional)</small>', $model->first_name) }}
    {{ Form::bsText('last_name', 'Last name <small>(optional)</small>', $model->last_name) }}
    @can('super')
    {{ Form::bsSelect('school_id', 'School', $model->school_id, $schoolList) }}
    @endcan
    <div class="form-group">
        <button type="submit" class="btn btn-primary btn-lg">Save</button>
    </div>
    {{ Form::close() }}

@endsection
