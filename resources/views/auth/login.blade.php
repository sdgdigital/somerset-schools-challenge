@extends('layouts.auth')

@section('title', 'Login')

@section('content')

    <div class="panel panel-default">
        <div class="panel-login">
            <div class="panel-heading">
                <h1 class="panel-title">{{ ucfirst(trans('app.login')) }} to the {{ trans('app.competition') }}</h1>
            </div>
            <div class="panel-body">
                {{ Form::open(['route' => 'login']) }}
                {{ Form::bsEmail('email', 'Email', old('email'), ['required', 'autofocus', 'placeholder' => 'Your email address']) }}
                {{ Form::bsPassword('password', 'Password', ['required', 'placeholder' => 'Your password']) }}
                <div class="form-group text-center">
                    {{ Form::bsCheckbox('remember', 'Remember me') }}
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-lg btn-block">{{ ucfirst(trans('app.login')) }}</button>
                    <a class="btn btn-link btn-block" href="{{ url('/password/reset') }}">Forgot your password?</a>
                    <a class="btn btn-link btn-block" href="{{ url('/register') }}">Not registered? Sign up!</a>
                </div>
                {{ Form::close() }}
            </div>
        </div>
        <div class="panel-warning hide">
            <div class="panel-heading">
                <h1 class="panel-title">Update your browser!</h1>
            </div>
            <div class="panel-body">
                The Somerset Schools Challenge website has been designed to work with modern web browsers. We have detected that you are not using one of our recommended browsers and would strongly suggest you ask your IT department to upgrade you.<br/><br/>Please use one of the links below for more information:<br/><br/>
                <div class="form-group">
                    <a class="btn btn-link btn-block" href='http://www.apple.com/safari' target='_blank'>Apple Safari</a>
                    <a class="btn btn-link btn-block" href='https://www.google.com/chrome' target='_blank'>Google Chrome</a>
                    <a class="btn btn-link btn-block" href='http://windows.microsoft.com/en-us/internet-explorer/download-ie' target='_blank'>Microsoft Internet Explorer</a>
                    <a class="btn btn-link btn-block" href='http://www.browserfordoing.com' target='_blank'>Microsoft Edge</a>
                    <a class="btn btn-link btn-block" href='https://www.mozilla.org/en-US/firefox/new' target='_blank'>Mozilla Firefox</a>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>

@endsection
