@extends('layouts.auth')

@section('title', 'Register')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">
            <h1 class="panel-title">{{ ucfirst(trans('app.register')) }} for the {{ trans('app.competition') }}</h1>
        </div>
        <div class="panel-body">
            {{ Form::open(['route' => 'register']) }}
            <div class="form-group{{ $errors->has('school_id') ? ' has-error' : '' }}">
                <label for="school_id">Your school</label>
                <select name="school_id" id="school_id" class="form-control">
                    <option value="">-- Please select --</option>
                    @foreach($localAuthorities as $localAuthority)
                        <!-- <optgroup label="{{ $localAuthority->name }}"> -->
                            @foreach($localAuthority->schools as $school)
                                <option value="{{ $school->id }}">{{ $school->name }}</option>
                            @endforeach
                        <!-- </optgroup> -->
                    @endforeach
                </select>
            </div>
            <hr>
            {{ Form::bsText('first_name', 'First name', old('first_name'), ['placeholder' => 'Your first name', 'required']) }}
            {{ Form::bsText('last_name', 'Last name', old('last_name'), ['placeholder' => 'Your last name', 'required']) }}
            {{ Form::bsText('display_name', 'Display name <small>(optional)</small>', old('display_name'), ['placeholder' => 'What shall we call you?']) }}
            {{ Form::bsEmail('email', 'Email', old('email'), ['placeholder' => 'What is your email address?', 'required']) }}
            {{ Form::bsPassword('password', 'Password', ['placeholder' => 'Create a password', 'required']) }}
            {{ Form::bsPassword('password_confirmation', 'Confirm password', ['placeholder' => 'Enter your password again', 'required']) }}
            <div class="form-group text-center">
                {{ Form::bsCheckbox('privacy_statement', 'I have read the <a href="' . url('/privacy-statement') . '">privacy statement</a>') }}
                {{ Form::bsCheckbox('accepted_terms', 'I accept the <a href="' . url('/terms-and-conditions') . '">terms and conditions</a>') }}
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-lg btn-block">{{ ucfirst(trans('app.register')) }}</button>
                <a class="btn btn-link btn-block" href="{{ route('login') }}"><span class="fa fa-arrow-left fa-fw"></span> Back to {{ trans('app.login') }}</a>
            </div>
            {{ Form::close() }}
        </div>
    </div>

@endsection
