@extends('layouts.auth')

@section('title', 'Reset password')

@section('content')

    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <div class="panel panel-default">
        <div class="panel-heading">
            <h1 class="panel-title">Reset password</h1>
        </div>
        <div class="panel-body">
            {{ Form::open(['url' => '/password/email']) }}
            {{ Form::bsEmail('email', 'Email', old('email'), ['required', 'autofocus']) }}
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-lg btn-block">Send Password Reset Link</button>
                <a class="btn btn-link btn-block" href="{{ route('login') }}"><span class="fa fa-arrow-left fa-fw"></span> Back to {{ trans('app.login') }}</a>
            </div>
            {{ Form::close() }}
        </div>
    </div>

@endsection
