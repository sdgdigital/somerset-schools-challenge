@extends('layouts.auth')

@section('title', 'Reset password')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">
            <h1 class="panel-title">Reset password</h1>
        </div>
        <div class="panel-body">
            {{ Form::open(['url' => '/password/reset']) }}
            {{ Form::hidden('token', $token) }}
            {{ Form::bsEmail('email', 'Email', $email or old('email'), ['required', 'autofocus']) }}
            {{ Form::bsPassword('password', 'Password', ['required']) }}
            {{ Form::bsPassword('password_confirmation', 'Confirm password', ['required']) }}
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-lg btn-block">Reset Password</button>
                <a class="btn btn-link btn-block" href="{{ route('login') }}"><span class="fa fa-arrow-left fa-fw"></span> Back to {{ trans('app.login') }}</a>
            </div>
            {{ Form::close() }}
        </div>
    </div>

@endsection
