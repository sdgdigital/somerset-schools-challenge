<!DOCTYPE html>
<html lang="en">
<head>
    <title>Test</title>
    <script   src="https://code.jquery.com/jquery-3.1.1.min.js"   integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="   crossorigin="anonymous"></script>
</head>
<body>
<form action="/app/trips" method="post">
    {{ csrf_field() }}
    <div>
        <label for="data">Data</label>
        <input type="text" name="data" value="" />
    </div>
    <button type="submit">Submit</button>
</form>
</body>
</html>