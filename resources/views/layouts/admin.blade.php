<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{-- Generated CSRF token (don't remove) --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield ('title') - {{ config('app.name') }}</title>

    <script src="https://use.typekit.net/{{ config('typekit.id') }}.js"></script>
    <script>try{Typekit.load({ async: true });}catch(e){}</script>
    <link href='https://fonts.googleapis.com/css?family=Quicksand' rel='stylesheet'>

    @section('styles-header')
        <link href="/css/admin.css" rel="stylesheet">
        <link href="/css/app.css" rel="stylesheet">
    @show

    @section('javascript-header')
        <script src="/js/admin.js"></script>
        <script>
            // Configure jQuery to submit the X-CSRF-TOKEN with every ajax call
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
            });
            window.Laravel = {!! json_encode(['csrfToken' => csrf_token()]) !!}

            // Set defaults for all datepickers
            // http://bootstrap-datepicker.readthedocs.org/en/latest/index.html
            $(function () {
                if (typeof $('.datepicker').datepicker !== undefined) {
                    $.fn.datepicker.defaults.autoclose = true;
                    $.fn.datepicker.defaults.todayHighlight = true;
                    $.fn.datepicker.defaults.format = "{{ $options->date_format_js }}";
                    $.fn.datepicker.defaults.startDate = "-365d";
                    $.fn.datepicker.defaults.endDate = "+365d";
                }
            });

            var GLOBAL_VARS = {
                @if (isset($competition))
                'competition_startdate': '{{ $competition->start_date->format($options->date_format) }}',
                @endif
                'api_path':              '/',
                'app_url':               '{{ config('app.url') }}',
                'app_name':              '{{ config('app.name') }}',
                'display_name':          '{{ !empty(auth()->user())?auth()->user()->display_name:"" }}',
                'distance_unit':         '{{ config('competition.distance_unit') }}',
                'distance_unit_abbr':    '{{ config('competition.distance_unit_abbr') }}',
                'date_format_js':        '{{ config('competition.distance_unit_abbr') }}'
            };
        </script>
    @show

    @stack('additional-header')
</head>
<body>
@include('partials.navbar')
<div class="container container-main">

    <div class="row">

        <main class="col-xs-12 main" role="main">
            <div class="c-logo"></div>
            @yield('content')
        </main>
    </div>
</div>
@include('partials.footer')
@include('partials.browserdetect')
@include('partials.google-analytics')
</body>
</html>
