<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{-- Generated CSRF token (don't remove) --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield ('title') - {{ config('app.name') }}</title>

    <script src="https://use.typekit.net/{{ config('typekit.id') }}.js"></script>
    <script>try{Typekit.load({ async: true });}catch(e){}</script>
    <link href='https://fonts.googleapis.com/css?family=Quicksand' rel='stylesheet'>

    @section('styles-header')
        <link href="/css/default.css" rel="stylesheet">
    @show

</head>
<body>
<main class="container" role="main">
    <div class="row">
        <div class="col-xs-12">
            <a href="{{ url('/') }}">
                <img height="auto" width="300" class="auth-logo img-responsive" src="{{ asset('img/default/logo.png') }}" alt="">
            </a>
            @yield('content')
        </div>
    </div>
</main>
@include('partials.footer')
@include('partials.google-analytics')
</body>
</html>
