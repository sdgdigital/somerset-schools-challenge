<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{-- Generated CSRF token (don't remove) --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield ('title') - {{ config('app.name') }}</title>

    @section('styles-header')
        <link href="/css/default.css" rel="stylesheet">
    @show

    @section('javascript-header')
        <script>window.Laravel = <?php echo json_encode(['csrfToken' => csrf_token()]); ?></script>
    @show

</head>
<body>
<main class="container" role="main">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
            <div class="row">
                <div class="col-xs-8 col-xs-offset-2">
                    <a href="{{ url('/') }}">
                        <img class="auth-logo img-responsive" src="{{ asset('img/default/logo.png') }}" alt="">
                    </a>
                </div>
            </div>
            @yield('content')
        </div>
    </div>
</main>
@include('partials.browserdetect')
@include('partials.google-analytics')
</body>
</html>
