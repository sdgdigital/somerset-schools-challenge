<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Somerset Schools Challenge</title>

    <script src="https://use.typekit.net/{{ config('typekit.id') }}.js"></script>
    <script>try{Typekit.load({ async: true });}catch(e){}</script>
    <link href='https://fonts.googleapis.com/css?family=Quicksand' rel='stylesheet'>

    <script src="js/lib/jquery/jquery.min.js"></script>
    <script src="js/lib/bootpag/jquery.bootpag.min.js"></script>
    <script src="js/lib/chartit/chartist.js"></script>
    <script src="js/lib/chartit/chartist-plugin-pointlabels.min.js"></script>
    
    <link href="js/lib/chartit/chartist.css" rel="stylesheet" type="text/css">
   

    <link href="/css/app.css" rel="stylesheet">
    <link href="js/lib/introjs/introjs.css" rel="stylesheet">
    <script type="text/javascript" src="js/lib/introjs/intro.js"></script>

    <script>
        window.GLOBAL_VARS = {
            @if (isset($competition))
            'competition_startdate': '{{ $competition->start_date->format($options->date_format) }}',
            @endif
            'api_path':              '/',
            'app_url':               '{{ config('app.url') }}',
            'app_name':              '{{ config('app.name') }}',
            'display_name':          '{{ !empty(auth()->user())?auth()->user()->display_name:"" }}',
            'distance_unit':         '{{ config('competition.distance_unit') }}',
            'distance_unit_abbr':    '{{ config('competition.distance_unit_abbr') }}',
            'date_format_js':        '{{ config('competition.distance_unit_abbr') }}',
            'csrf_token':            '{{ csrf_token() }}'
        };
    </script>
</head>
<body>
@yield('content')

@include('partials.google-analytics')    
</body>
</html>
