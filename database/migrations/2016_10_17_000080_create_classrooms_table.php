<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassroomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classrooms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->unsignedInteger('school_id')->nullable();
            $table->foreign('school_id')->references('id')->on('schools');
            $table->string('year_group')->default('reception');
            $table->integer('number_of_pupils')->nullable();
            $table->integer('walk')->default(0);
            $table->integer('bus')->default(0);
            $table->integer('bike')->default(0);
            $table->integer('car')->default(0);
            $table->integer('train')->default(0);
            $table->integer('scooter')->default(0);
            $table->integer('share')->default(0);
            $table->integer('park')->default(0);
            $table->integer('taxi')->default(0);
            $table->integer('wheelchair')->default(0);
            $table->integer('total_trips')->default(0);
            $table->integer('total_points')->default(0);
            $table->integer('total_sustainable_points')->default(0);
            $table->decimal('percent_sustainable_trips', 4, 1)->default('0.0');
            $table->decimal('participation', 10, 4)->default('0.0000');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('classrooms');
    }
}
