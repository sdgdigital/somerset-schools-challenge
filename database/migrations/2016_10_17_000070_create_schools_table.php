<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schools', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->unsignedInteger('local_authority_id')->default(1);
            $table->foreign('local_authority_id')->references('id')->on('local_authorities');
            $table->integer('walk')->default(0);
            $table->integer('bus')->default(0);
            $table->integer('bike')->default(0);
            $table->integer('car')->default(0);
            $table->integer('train')->default(0);
            $table->integer('scooter')->default(0);
            $table->integer('share')->default(0);
            $table->integer('park')->default(0);
            $table->integer('taxi')->default(0);
            $table->integer('wheelchair')->default(0);
            $table->integer('total_trips')->default(0);
            $table->integer('total_points')->default(0);
            $table->integer('total_sustainable_points')->default(0);
            $table->decimal('percent_sustainable_trips', 4, 1)->default('0.0');
            $table->decimal('percent_participation', 10, 4)->default('0.0000');
            $table->decimal('score')->default(0);
            $table->string('postcode')->nullable();
            $table->string('eastings')->nullable();
            $table->string('northings')->nullable();
            $table->integer('number_of_pupils')->nullable();
            $table->integer('number_of_classes')->nullable();
            $table->string('phase_of_education')->nullable();
            $table->integer('number_of_pt_stops_within_400_meters')->nullable();
            $table->integer('pt_service_frequency_7am_to_830am')->nullable();
            $table->integer('rank_for_pt_stops')->nullable();
            $table->integer('rank_for_frequency')->nullable();
            $table->integer('combined_rank')->nullable();
            $table->integer('overall_rank')->nullable();
            $table->string('accessibility_marker')->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('schools');
    }
}
