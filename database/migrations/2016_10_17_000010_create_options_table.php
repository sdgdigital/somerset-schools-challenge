<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOptionsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('options', function (Blueprint $table) {
      $table->increments('id');
      $table->string('date_format')->default('Y-m-d');
      $table->string('date_format_js')->default('yyyy-mm-dd');
      $table->string('date_format_pretty')->default('j F, \a\t h:i');
      $table->string('date_format_short')->default('j F');
      $table->string('distance_unit')->default('mile');
      $table->string('distance_unit_abbr')->default('mi');
      $table->string('weight_unit')->default('pound');
      $table->string('weight_unit_abbr')->default('lb');
      $table->string('google_code')->nullable();
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop('options');
  }
}
