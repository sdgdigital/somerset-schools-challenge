<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('users', function (Blueprint $table) {
      $table->increments('id');
      $table->string('email')->unique();
      $table->unsignedInteger('school_id')->nullable();
      $table->foreign('school_id')->references('id')->on('schools');
      $table->string('display_name')->unique()->nullable();
      $table->string('first_name')->nullable();
      $table->string('last_name')->nullable();
      $table->boolean('is_admin')->default(0);
      $table->boolean('is_super')->default(0);
      $table->timestamp('last_login')->nullable();
      $table->string('last_login_ip')->nullable();
      $table->string('password');
      $table->rememberToken();
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop('users');
  }
}
