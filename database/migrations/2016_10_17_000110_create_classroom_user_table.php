<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassroomUserTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('classroom_user', function (Blueprint $table) {
      $table->unsignedInteger('classroom_id')->default(0);
      $table->foreign('classroom_id')->references('id')->on('classrooms');
      $table->unsignedInteger('user_id')->default(0);
      $table->foreign('user_id')->references('id')->on('users');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop('classroom_user');
  }
}
