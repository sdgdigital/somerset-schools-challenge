<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTripsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('trips', function (Blueprint $table) {
      $table->increments('id');
      $table->unsignedInteger('competition_id')->nullable();
      $table->foreign('competition_id')->references('id')->on('competitions');
      $table->unsignedInteger('classroom_id')->nullable();
      $table->foreign('classroom_id')->references('id')->on('classrooms');
      $table->date('date');
      $table->integer('walk')->default(0);
      $table->integer('bus')->default(0);
      $table->integer('bike')->default(0);
      $table->integer('car')->default(0);
      $table->integer('train')->default(0);
      $table->integer('scooter')->default(0);
      $table->integer('share')->default(0);
      $table->integer('park')->default(0);
      $table->integer('taxi')->default(0);
      $table->integer('wheelchair')->default(0);
      $table->integer('total_trips')->default(0);
      $table->integer('total_points')->default(0);
      $table->integer('total_sustainable_points')->default(0);
      $table->decimal('percent_sustainable_trips', 4, 1)->default('0.0');
      $table->unsignedInteger('weather_id')->nullable();
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop('trips');
  }
}
