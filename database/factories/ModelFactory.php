<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Option::class, function (Faker\Generator $faker) {
  return [
    'date_format'        => 'Y-m-d',
    'date_format_js'     => 'yyyy-mm-dd',
    'date_format_pretty' => 'j F, \a\t h:i',
    'date_format_short'  => 'j F',
    'distance_unit_abbr' => 'km',
    'weight_unit_abbr'   => 'kg',
    'created_at'         => new DateTime(),
  ];
});

$factory->define(App\Mode::class, function (Faker\Generator $faker) {
  return [
    'name'       => '',
    'class_name' => '',
    'points'     => 0,
  ];
});

$factory->define(App\Weather::class, function (Faker\Generator $faker) {
  return [
    'name'       => '',
    'class_name' => '',
  ];
});

$factory->define(App\Competition::class, function (Faker\Generator $faker) {
  return [
    'name'        => 'Test',
    'description' => 'Lorem ipsum dolor',
  ];
});

$factory->define(App\LocalAuthority::class, function (Faker\Generator $faker) {
  return [
    'name' => $faker->company,
  ];
});

$factory->define(App\School::class, function (Faker\Generator $faker) {
  return [
    'name' => $faker->company,
  ];
});

$factory->define(App\User::class, function (Faker\Generator $faker) {
  static $password;

  $lastName = $faker->lastName;

  return [
    'email'        => $faker->unique()->safeEmail,
    'first_name'   => $faker->firstName,
    'last_name'    => $lastName,
    'display_name' => 'Mrs. ' . $lastName,
    'password'     => $password ?: $password = bcrypt('password'),
  ];
});

$factory->define(App\Classroom::class, function (Faker\Generator $faker) {
  return [
    'name'             => $faker->company,
    'number_of_pupils' => rand(20, 30),
  ];
});

$factory->define(App\Trip::class, function (Faker\Generator $faker) {

  $today = app('today');

  return [
    'competition_id' => 1,
    'classroom_id'   => 1,
    'date'           => $today->format(app('options')->date_format),
    'walk'           => 0,
    'bus'            => 0,
    'bike'           => 0,
    'car'            => 0,
    'train'          => 0,
    'scooter'        => 0,
    'share'          => 0,
    'park'           => 0,
    'taxi'           => 0,
    'wheelchair'     => 0,
    'weather_id'     => 0,
  ];
});
