<?php

use Illuminate\Database\Seeder;

class CompetitionsTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    factory(App\Competition::class, 1)->create([
      'name'         => 'Test Competition',
      'description'  => 'A test competition to work with',
      'is_active'    => 1,
      'start_date'   => app('today')->subDay(3)->format(app('options')->date_format),
      'end_date'     => app('today')->addDay(3)->format(app('options')->date_format),
      'grace_period' => app('today')->addDay(6)->format(app('options')->date_format),
    ]);
  }
}
