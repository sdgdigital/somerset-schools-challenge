<?php

use Illuminate\Database\Seeder;

class WeatherTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    factory(App\Weather::class, 1)->create([
      'name'       => 'Sunny',
      'class_name' => 'sunny',
    ]);

    factory(App\Weather::class, 1)->create([
      'name'       => 'Cloudy',
      'class_name' => 'cloudy',
    ]);

    factory(App\Weather::class, 1)->create([
      'name'       => 'Partly cloudy',
      'class_name' => 'partly',
    ]);

    factory(App\Weather::class, 1)->create([
      'name'       => 'Rainy',
      'class_name' => 'rainy',
    ]);

    factory(App\Weather::class, 1)->create([
      'name'       => 'Stormy',
      'class_name' => 'stormy',
    ]);

    factory(App\Weather::class, 1)->create([
      'name'       => 'Snowy',
      'class_name' => 'snowy',
    ]);

    factory(App\Weather::class, 1)->create([
      'name'       => 'Foggy',
      'class_name' => 'foggy',
    ]);

    factory(App\Weather::class, 1)->create([
      'name'       => 'Windy',
      'class_name' => 'windy',
    ]);
  }
}
