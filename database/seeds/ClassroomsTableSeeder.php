<?php

use Illuminate\Database\Seeder;

class ClassroomsTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    factory(App\Classroom::class, 1)->create([
      'name'      => 'Classroom 1',
      //'school_id' => 1,
    ]);
    factory(App\Classroom::class, 1)->create([
      'name'      => 'Classroom 2',
    ]);
    factory(App\Classroom::class, 1)->create([
      'name'      => 'Classroom 3',
    ]);
    factory(App\Classroom::class, 1)->create([
      'name'      => 'Classroom 4',
    ]);
  }
}
