<?php

use Illuminate\Database\Seeder;

class TripsTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @param void
   */
  public function run()
  {
    factory(App\Trip::class, 1)->create([
      'competition_id' => 1,
      'classroom_id'   => 1,
      'date'           => app('today')->format('Y-m-d'),
      'walk'           => 3,
      'bus'            => 2,
      'bike'           => 6,
      'car'            => 6,
      'train'          => 4,
      'scooter'        => 2,
      'share'          => 3,
      'park'           => 1,
      'taxi'           => 0,
      'wheelchair'     => 3,
      'weather_id'     => 0,
    ]);
  }
}

