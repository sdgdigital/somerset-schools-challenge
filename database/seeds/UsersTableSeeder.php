<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    factory(App\User::class, 1)->create([
      'email'     => 'user1@example.com',
      'school_id' => 1,
      'is_admin'  => 1,
    ]);
    factory(App\User::class, 1)->create([
      'email'     => 'user2@example.com',
      'school_id' => 1,
    ]);
    factory(App\User::class, 1)->create([
      'email'     => 'user3@example.com',
      'school_id' => 2,
    ]);
    factory(App\User::class, 1)->create([
      'email'     => 'user4@example.com',
      'school_id' => 2,
    ]);
    factory(App\User::class, 1)->create([
      'email'    => 'super@example.com',
      'is_super' => 1,
    ]);
  }
}
