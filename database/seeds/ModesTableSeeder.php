<?php

use Illuminate\Database\Seeder;

class ModesTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @param void
   */
  public function run()
  {
    factory(App\Mode::class, 1)->create([
      'name'       => 'Walk',
      'class_name' => 'walk',
      'points'     => 3,
    ]);

    factory(App\Mode::class, 1)->create([
      'name'       => 'Bus',
      'class_name' => 'bus',
      'points'     => 2,
    ]);

    factory(App\Mode::class, 1)->create([
      'name'       => 'Bike',
      'class_name' => 'bike',
      'points'     => 3,
    ]);

    factory(App\Mode::class, 1)->create([
      'name'       => 'Car',
      'class_name' => 'car',
      'points'     => 0,
    ]);

    factory(App\Mode::class, 1)->create([
      'name'       => 'Train',
      'class_name' => 'train',
      'points'     => 2,
    ]);

    factory(App\Mode::class, 1)->create([
      'name'       => 'Scooter',
      'class_name' => 'scooter',
      'points'     => 3,
    ]);

    factory(App\Mode::class, 1)->create([
      'name'       => 'Car with friends',
      'class_name' => 'share',
      'points'     => 1,
    ]);

    factory(App\Mode::class, 1)->create([
      'name'       => 'Park & Stride',
      'class_name' => 'park',
      'points'     => 1,
    ]);

    factory(App\Mode::class, 1)->create([
      'name'       => 'Taxi',
      'class_name' => 'taxi',
      'points'     => 1,
    ]);

    factory(App\Mode::class, 1)->create([
      'name'       => 'Wheelchair',
      'class_name' => 'wheelchair',
      'points'     => 3,
    ]);
  }
}

