<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $this->call(OptionsTableSeeder::class);
    $this->call(ModesTableSeeder::class);
    $this->call(WeatherTableSeeder::class);
    $this->call(PagesTableSeeder::class);
    $this->call(CompetitionsTableSeeder::class);
    $this->call(LocalAuthoritiesTableSeeder::class);
    $this->call(SchoolsTableSeeder::class);
    $this->call(ClassroomsTableSeeder::class);
    $this->call(UsersTableSeeder::class);
    //$this->call(TripsTableSeeder::class);
  }
}
