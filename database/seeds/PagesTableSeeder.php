<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    // markdown for terms & conditions page
    $terms_and_conditions = File::get('pages/terms_and_conditions.md');

    DB::table('pages')->insert([
      'name'       => 'Terms and Conditions',
      'url'        => '/terms-and-conditions',
      'body'       => $terms_and_conditions,
      'created_at' => new DateTime,
    ]);

    // markdown for how it works page
    $how_it_works = File::get('pages/how_it_works.md');

    DB::table('pages')->insert([
      'name'       => 'How it Works',
      'url'        => '/how-it-works',
      'body'       => $how_it_works,
      'created_at' => new DateTime,
    ]);
  }
}
