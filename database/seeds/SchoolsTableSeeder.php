<?php

use Illuminate\Database\Seeder;

class SchoolsTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    factory(App\School::class, 1)->create([
      'name'               => 'School 1',
      'local_authority_id' => 1,
    ]);
    factory(App\School::class, 1)->create([
      'name'               => 'School 2',
      'local_authority_id' => 1,
    ]);
  }
}
