<?php

use Illuminate\Database\Seeder;

class LocalAuthoritiesTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    factory(App\LocalAuthority::class, 1)->create([
      'name' => 'Local authority 1',
    ]);
  }
}
