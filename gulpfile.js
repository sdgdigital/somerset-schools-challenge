const elixir = require('laravel-elixir');
const sheets2json = require('gulp-sheets2json');
const insert = require('gulp-insert');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(mix => {
	
	// convert excel file to json
	// gulp
	// 	.src("./resources/data/questions.xlsx")
	// 	.pipe(sheets2json())
	// 	.pipe(insert.prepend('"data": '))
	// 	.pipe(insert.prepend('{'))
	// 	.pipe(insert.append('}'))
	// 	.pipe(gulp.dest('./public/data/'))
	// 	.pipe(gulp.dest('./resources/data/'));
	
	mix
		// default theme
		//.sass('default.scss')
		
		// app
		//.sass('app.scss')
		.webpack('app.js')
		
		// admin
		//.sass('admin.scss')
		.webpack('admin.js')
		
		// fonts
		// .copy('node_modules/bootstrap-sass/assets/fonts/bootstrap', 'public/fonts/bootstrap')
		// .copy('node_modules/font-awesome/fonts', 'public/fonts')
		
		// images
		// .copy('resources/assets/img', 'public/img')
		
		// auto-refresh browser - http://localhost:3000
		// .browserSync({
		// 	proxy: 'schools-platform.dev'
		// })
	
		;
	
	// mix.copy('./resources/assets/img', 'public/img');
	// mix.copy('./resources/assets/js/lib', 'public/js/lib');
});
