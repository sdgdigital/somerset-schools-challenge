# Somerset Schools Challenge

This is the Laravel/Vue powered application that allows select businesses to collect data about freight deliveries. It comprises of a initial survey and then a survey for each delivery throughout the day.

### Prerequisites
- PHP 7+
- PHP extension: OpenSSL, PDO, Mbstring, Tokenizer, XML
- MySQL 5.7+
- Node 6+
- NPM 3+
- Composer 1+

### Installation
```bash
$ composer install
$ npm install
```

### Permissions
```bash
$ chmod -R 777 bootstrap/cache storage
```

### Database
```bash
mysql> CREATE DATABASE IF NOT EXISTS `tfl_consolidation`;
mysql> CREATE USER 'usr_tfl_consolidation'@'localhost' IDENTIFIED BY 'myPassword';
mysql> GRANT ALL PRIVILEGES ON tfl_consolidation.* To 'usr_tfl_consolidation'@'localhost';
```

### Environment
Create a .env file and update the database details accordingly
```bash
$ cp .env.example .env
```

### App key
```bash
$ php artisan key:generate
```

### Populate database
```bash
$ php artisan migrate
```

### Author
Colin Woods

More information about setting up Laravel can be found here:
[ttps://laravel.com/docs/5.5#installation](https://laravel.com/docs/5.5#installation)