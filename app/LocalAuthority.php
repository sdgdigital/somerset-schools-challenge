<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LocalAuthority extends Model
{
  use SoftDeletes;

  protected $fillable = [
    'name',
  ];

  protected $hidden = [
    'created_at',
    'updated_at',
    'deleted_at',
  ];

  //region Relations

  public function schools()
  {
    return $this->hasMany(School::class);
  }

  public function classrooms()
  {
    return $this->hasManyThrough(School::class, Classroom::class);
  }

  //endregion

}
