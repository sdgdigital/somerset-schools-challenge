<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

use App\User;
use App\Classroom;
use App\LocalAuthority;

class ClassroomsController extends Controller
{
  protected $resource = 'classrooms';

  /**
   * Create a new controller instance.
   *
   * @return mixed
   */
  public function __construct()
  {
    // share a variable with all the views
    view()->share('resource', $this->resource);
  }

  /**
   * Display a listing of the resource.
   *
   * @param $request
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    // authorize the user (pass in an empty model)
    $this->authorize('create', Classroom::class);

    // retrieve the collection
    $collection = Classroom::where(function ($query) {
        if (app('user')->cannot('super')) {
          $query->where('school_id', '=', app('user')->school_id);
        }
      })
      ->orderBy('school_id', 'asc')
      ->orderBy('name', 'asc')
      ->paginate(25);

    // check if user has assigned class
    $assigned = false;
    if (app('user')->cannot('super')) {
      foreach ($collection as $model) {
        if(app('user')->id == $model['teacher']['id']){
          $assigned = true; break;
        }
      }
    }

    // return the view
    return view('admin.index', compact('collection', 'assigned'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    // authorize the user (pass in an empty model)
    $this->authorize('create', Classroom::class);

    if (app('user')->isSuper() && empty(app('user')->school_id)) {
      flash()->warning("<strong>Warning!</strong> Before you can create a classroom you need to belong to a school. Please choose a school and try again.");
      return redirect()->route('my-account.edit');
    }

    // get a list of teachers
    $userList = User::selectList(app('user')->school_id);

    // get a list of year groups
    $yearGroupList = Classroom::selectYearGroup();

    // return the view
    return view('admin.create', compact('userList', 'yearGroupList'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Requests\ClassroomRequest $request)
  {
    // authorize the user (pass in an empty model)
    $this->authorize('create', Classroom::class);

    // create the record
    $model = Classroom::create($request->all());

    // flash a success message
    flash()->success('Classroom created successfully!');

    // save the relation
    if ($request->has('teacher_id')) {
      $model->load('teachers');
      $model->teachers()->sync([$request->input('teacher_id')]);
    }

    // redirect
    return redirect()->route('classrooms.index');
  }

  /**
   * Display the specified resource.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    return redirect()->route($this->resource . '.edit', $id);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    // retrieve the model
    $model = Classroom::with('teachers')
      ->findOrFail($id);

    // authorize the user
    $this->authorize('update', $model);

    // get a list of teachers
    $userList = User::selectList($model->school_id, $model->teachers[0]->id);

    // get a list of year groups
    $yearGroupList = Classroom::selectYearGroup();

    // return the view
    return view('admin.edit', compact('model', 'userList', 'yearGroupList'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  $request
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function update(Requests\ClassroomRequest $request, $id)
  {
    // retrieve the model
    $model = Classroom::findOrFail($id);

    // authorize the user
    $this->authorize('update', $model);

    // update the record
    $model->update($request->all());

    // save the relation
    if ($request->has('teacher_id')) {
      $model->load('teachers');
      $model->teachers()->sync([$request->input('teacher_id')]);
    }

    // flash a success message
    flash()->success('Classroom updated successfully!');

    // redirect
    return redirect()->route('classrooms.edit', $model->id);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    // retrieve the model
    $model = Classroom::findOrFail($id);

    // authorize the user
    $this->authorize('delete', $model);

    // delete the model
    $model->delete();

    // flash a success message
    flash()->success('Classroom deleted successfully!');

    // redirect
    return redirect()->route('classrooms.index');
  }
}
