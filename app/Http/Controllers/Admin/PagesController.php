<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

use App\Page;

class PagesController extends Controller
{
  protected $resource = 'pages';

  /**
   * Create a new controller instance.
   *
   * @return mixed
   */
  public function __construct()
  {
    // share a variable with all the views
    view()->share('resource', $this->resource);
  }

  /**
   * Display a listing of the resource.
   *
   * @param Request $request
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    // authorize the user (pass in an empty model)
    $this->authorize('create', Page::class);

    // retrieve the collection
    $collection = Page::paginate();

    return view('admin.index', compact('collection'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    // retrieve the model
    $model = Page::findOrFail($id);

    // authorize the user
    $this->authorize('update', $model);

    // return the view
    return view('admin.edit', compact('model'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  $request
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function update(Requests\PageRequest $request, $id)
  {
    // retrieve the model
    $model = Page::findOrFail($id);

    // authorize the user
    $this->authorize('update', $model);

    // update the record
    $model->update($request->all());

    // flash a success message
    flash()->success('Page updated successfully!');

    // redirect
    return redirect()->route('pages.edit', $model->id);
  }

}
