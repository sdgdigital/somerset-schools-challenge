<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Storage;
use DB;
use Carbon\Carbon;

class ReportController extends Controller
{
    protected $resource = 'reports';

    public function __construct()
    {
        // output path for the exports
        //$this->reports_path = storage_path('app/public/reports');
	$this->reports_path = '/tmp/mysqlfiles/';
	$this->filetimestamp = Carbon::now()->timestamp;


        // share a variable with all the views
        view()->share('resource', $this->resource);
    }

    public function getIndex()
    {
        if (request()->user()->cannot('super')) {
            abort(403, 'This action is unauthorized.');
        }

        return view('admin.edit');
    }

    public function getUsersReport()
    {

        if (request()->user()->cannot('super')) {
            abort(403, 'This action is unauthorized.');
        }

        $file_name  = 'users-' . $this->filetimestamp . '.csv';
        $full_path  = $this->reports_path . '/' . $file_name;
        $local_path = 'public/reports/' . $file_name;

        $sql = "
            -- define column names
            SELECT
                'id',
                'email',
                'local_authority',
                'school',
                'administrator',
                'site_administrator',
                'date_created',
                'last_login'
      
            -- insert column names
            UNION ALL

            SELECT
                u.id,
                coalesce(u.email, ''),
                (
                    SELECT 
                        l.name
                        FROM
                        local_authorities l
                        LEFT JOIN
                        schools s ON l.id = s.local_authority_id
                        WHERE
                        l.id = s.id
                ) AS local_authority,
                (
                    SELECT
                        s.name
                        FROM 
                        schools s
                        WHERE
                        s.id = u.school_id
                ) AS school,
                if(u.is_admin = 0, 'no', 'yes') AS administrator,
                if(u.is_super = 0, 'no', 'yes') AS super_administrator,
                coalesce(u.created_at, '') AS date_created,
                coalesce(u.last_login, '') AS last_login
                
                -- define the output location & options
                INTO OUTFILE
                    \"" . $full_path . "\"
                    FIELDS TERMINATED BY ','
                    OPTIONALLY ENCLOSED BY '\"'
                    LINES TERMINATED BY '\n'
                
                FROM
                    users u
        ";

        // delete any existing file
        $exists = Storage::exists($local_path);
        if ($exists) {
            Storage::delete($local_path);
        }

        // create the CSV
        DB::select($sql);

        return response()->download($full_path)->deleteFileAfterSend(true);
    }

    public function getSchoolsReport()
    {
        if (request()->user()->cannot('super')) {
            abort(403, 'This action is unauthorized.');
        }

        $file_name  = 'schools.csv';
        $file_name  = 'schools-' . $this->filetimestamp . '.csv';

        $full_path  = $this->reports_path . '/' . $file_name;
        $local_path = 'public/reports/' . $file_name;

        $sql = "
            -- define column names
            SELECT
                'id',
                'local_authority',
                'school',
                'number_of_pupils',
                'walk',
                'bus',
                'bike',
                'car',
                'train',
                'scooter',
                'share',
                'park_stride',
                'taxi',
                'wheelchair',
                'total_trips',
                'percent_healthy_trips',
                'percent_participation',
                'score'
      
            -- insert column names
            UNION ALL

            SELECT
                s.id,
                (
                    SELECT 
                        l.name
                        FROM
                        local_authorities l
                        LEFT JOIN
                        schools s ON l.id = s.local_authority_id
                        WHERE
                        l.id = s.id
                ) AS local_authority,
                s.name AS school,
                s.number_of_pupils as number_of_pupils,
                s.walk as walk,
                s.bus as bus,
                s.bike as bike,
                s.car as car,
                s.train as train,
                s.scooter as scooter,
                s.share as share,
                s.park as park_stride,
                s.taxi as taxi,
                s.wheelchair as wheelchair,
                s.total_trips as total_trips,
                s.percent_sustainable_trips as percent_healthy_trips,
                s.percent_participation as percent_participation,
                s.score as score
                
                -- define the output location & options
                INTO OUTFILE
                    \"" . $full_path . "\"
                    FIELDS TERMINATED BY ','
                    OPTIONALLY ENCLOSED BY '\"'
                    LINES TERMINATED BY '\n'
                
                FROM
                    schools s
        ";

        // delete any existing file
        $exists = Storage::exists($local_path);
        if ($exists) {
            Storage::delete($local_path);
        }

        // create the CSV
        DB::select($sql);

        return response()->download($full_path);
    }

    public function getTripsReport()
    {
        if (request()->user()->cannot('super')) {
            abort(403, 'This action is unauthorized.');
        }

        $file_name  = 'trips.csv';
        $file_name  = 'trips-' . $this->filetimestamp . '.csv';

        $full_path  = $this->reports_path . '/' . $file_name;
        $local_path = 'public/reports/' . $file_name;

        $sql = "
            -- define column names
            SELECT
                'id',
                'competition_id',
                'date',
                'local_authority',
                'school',
                'classroom',
                'walk',
                'bus',
                'bike',
                'car',
                'train',
                'scooter',
                'share',
                'park_stride',
                'taxi',
                'wheelchair',
                'points',
                'healthy_points',
                'total_trips',
                'weather'
      
            -- insert column names
            UNION ALL

            SELECT
                t.id,
                t.competition_id as competition_id,
                t.date as date,
                l.name as local_authority,
                s.name as school,
                c.name as classroom,
                t.walk as walk,
                t.bus as bus,
                t.bike as bike,
                t.car as car,
                t.train as train,
                t.scooter as scooter,
                t.share as share,
                t.park as park_stride,
                t.taxi as taxi,
                t.wheelchair as wheelchair,
                t.total_points as points,
                t.total_sustainable_points as healthy_points,
                t.total_trips as total_trips,
                coalesce(w.name, '') as weather
                
            -- define the output location & options
            INTO OUTFILE
                \"" . $full_path . "\"
                FIELDS TERMINATED BY ','
                OPTIONALLY ENCLOSED BY '\"'
                LINES TERMINATED BY '\n'
            
            FROM
                trips t
                
            LEFT JOIN
                classrooms c ON t.classroom_id = c.id
                
            LEFT JOIN
                schools s ON c.school_id = s.id
                
            LEFT JOIN
                local_authorities l ON s.local_authority_id = l.id
                
            LEFT JOIN
                weather w ON t.weather_id = w.id
        ";

        // delete any existing file
        $exists = Storage::exists($local_path);
        if ($exists) {
            Storage::delete($local_path);
        }

        // create the CSV
        DB::select($sql);

        return response()->download($full_path);
    }

    public function getYearGroupReport()
    {
        if (request()->user()->cannot('super')) {
            abort(403, 'This action is unauthorized.');
        }

        $file_name  = 'year-group.csv';
        $file_name  = 'year-group-' . $this->filetimestamp . '.csv';

        $full_path  = $this->reports_path . '/' . $file_name;
        $local_path = 'public/reports/' . $file_name;

        $sql = "
            -- define column names
            SELECT
                'school',
                'classroom',
                'year_group'
            
            -- insert column names
            UNION ALL

            SELECT
                s.name,
                c.name,
                c.year_group
            
            -- define the output location & options
            INTO OUTFILE
                \"" . $full_path . "\"
                FIELDS TERMINATED BY ','
                OPTIONALLY ENCLOSED BY '\"'
                LINES TERMINATED BY '\n'
                
            FROM
                classrooms c
                  
            LEFT JOIN
                schools s ON c.school_id = s.id
            
            WHERE EXISTS
                (
                    SELECT *
                    FROM
                        trips t
                    WHERE
                        t.classroom_id = c.id AND
                        t.competition_id = " . app('competition')->id . " AND
                        t.deleted_at IS NULL
                )
        
        ";

        // delete any existing file
        $exists = Storage::exists($local_path);
        if ($exists) {
            Storage::delete($local_path);
        }

        // create the CSV
        DB::select($sql);

        return response()->download($full_path);
    }

}
