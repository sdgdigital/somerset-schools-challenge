<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

use App\Mode;

class ModesController extends Controller
{
  protected $resource = 'modes';

  /**
   * Create a new controller instance.
   *
   * @return mixed
   */
  public function __construct()
  {
    // share a variable with all the views
    view()->share('resource', $this->resource);
  }

  /**
   * Display a listing of the resource.
   *
   * @param Request $request
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    // authorize the user (pass in an empty model)
    $this->authorize('create', Mode::class);

    // retrieve the collection
    $collection = Mode::paginate();

    // return the view
    return view('admin.index', compact('collection'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Requests\ModeRequest $request)
  {
    // authorize the user (pass in an empty model)
    $this->authorize('create', Mode::class);

    // create the record
    $model = Mode::create($request->all());

    // flash a success message
    flash()->success('Mode created successfully!');

    // redirect
    return redirect()->route('modes.edit', $model->id);
  }

  /**
   * Display the specified resource.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    return redirect()->route($this->resource . '.edit', $id);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    // retrieve the model
    $model = Mode::findOrFail($id);

    // authorize the user
    $this->authorize('update', $model);

    // return the view
    return view('admin.edit', compact('model'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  $request
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function update(Requests\ModeRequest $request, $id)
  {
    // retrieve the model
    $model = Mode::findOrFail($id);

    // authorize the user
    $this->authorize('update', $model);

    // update the record
    $model->update($request->all());

    // flash a success message
    flash()->success('Mode updated successfully!');

    // redirect
    return redirect()->route('modes.edit', $model->id);
  }
}
