<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

use App\Competition;
use App\Mode;
use Illuminate\Support\Facades\Artisan;

class CompetitionsController extends Controller
{
    protected $resource = 'competitions';

    /**
     * Create a new controller instance.
     *
     * @return mixed
     */
    public function __construct()
    {
        // share a variable with all the views
        view()->share('resource', $this->resource);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // authorize the user (pass in an empty model)
        $this->authorize('create', Competition::class);

        // retrieve the collection
        $collection = Competition::paginate();

        // return the view
        return view('admin.index', compact('collection'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // authorize the user (pass in an empty model)
        $this->authorize('create', Competition::class);

        // get the available modes
        $modes = Mode::all();

        // return the view
        return view('admin.create', compact('modes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\CompetitionRequest $request)
    {
        // authorize the user (pass in an empty model)
        $this->authorize('create', Competition::class);

        // create the record
        $model = Competition::create($request->all());

        // flash a success message
        flash()->success('Competition created successfully!');

        // clear the app cache
        Artisan::call('cache:clear');

        // redirect
        return redirect()->route('competitions.edit', $model->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect()->route($this->resource . '.edit', $id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // retrieve the model
        $model = Competition::findOrFail($id);

        // authorize the user
        $this->authorize('update', $model);

        // return the view
        return view('admin.edit', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\CompetitionRequest $request, $id)
    {
        // retrieve the model
        $model = Competition::findOrFail($id);

        // authorize the user
        $this->authorize('update', $model);

        // update the record
        $model->update($request->all());

        // flash a success message
        flash()->success('Competition updated successfully!');

        // clear the app cache
        Artisan::call('cache:clear');

        // redirect
        return redirect()->route('competitions.edit', $model->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // retrieve the model
        $model = Competition::findOrFail($id);

        // authorize the user
        $this->authorize('delete', $model);

        // delete the model
        $model->delete();

        // flash a success message
        flash()->success('Competition deleted successfully!');

        // clear the app cache
        Artisan::call('cache:clear');

        // redirect
        return redirect()->route('competitions.index');
    }
}
