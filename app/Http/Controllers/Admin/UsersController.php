<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Mail;
use App\Mail\UserCreated;

use App\User;
use App\Classroom;
use App\School;

class UsersController extends Controller
{
  protected $resource = 'users';

  /**
   * Create a new controller instance.
   *
   * @return mixed
   */
  public function __construct()
  {
    // share a variable with all the views
    view()->share('resource', $this->resource);
  }

  /**
   * Display a listing of the resource.
   *
   * @param Request $request
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    // authorize the user (pass in an empty model)
    $this->authorize('create', User::class);

    $collection = User::where(function ($query) {
        if (app('user')->cannot('super')) {
          $query->where('school_id', '=', app('user')->school_id);
        }
      })
      ->orderBy('school_id', 'asc')
      ->paginate(25);

    // return the view
    return view('admin.index', compact('collection'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    // authorize the user (pass in an empty model)
    $this->authorize('create', User::class);

    $schoolList = School::selectList();

    // return the view
    return view('admin.create', compact('schoolList'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Requests\UserRequest $request)
  {
    // authorize the user (pass in an empty model)
    $this->authorize('create', User::class);

    $data = $request->all();
    $data['password'] = randomPassword();

    // create the record
    $model = User::create([
      'email'        => $data['email'],
      'password'     => bcrypt($data['password']),
      'display_name' => $data['display_name'],
      'first_name'   => $data['first_name'],
      'last_name'    => $data['last_name'],
      'school_id'    => $data['school_id'],
      'is_admin'     => 1,
    ]);

    // flash a success message
    flash()->success('User created successfully!');

    // send a confirmation email to the user
    Mail::to($data['email'])->send(new UserCreated($data));

    // redirect
    return redirect()->route('users.index');
  }

  /**
   * Display the specified resource.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    return redirect()->route($this->resource . '.edit', $id);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    // retrieve the model
    $model = User::findOrFail($id);

    // authorize the user
    $this->authorize('update', $model);

    $schoolList = School::selectList();

    // return the view
    return view('admin.edit', compact('model', 'schoolList'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  $request
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function update(Requests\UserRequest $request, $id)
  {
    // retrieve the model
    $model = User::findOrFail($id);

    // authorize the user
    $this->authorize('update', $model);

    // update the record
    $model->update($request->all());

    // flash a success message
    flash()->success('User updated successfully!');

    // redirect
    return redirect()->route('users.edit', $model->id);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $user = User::findOrFail($id);

    $this->authorize('delete', $user);

    $user->delete();

    return redirect()->route('users.index');
  }
}
