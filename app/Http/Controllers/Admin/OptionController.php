<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

use Artisan;
use App\Option;

class OptionController extends Controller {
	protected $resource = 'options';

	/**
	 * Create a new controller instance.
	 *
	 * @return mixed
	 */
	public function __construct() {
		// share a variable with all the views
		view()->share( 'resource', $this->resource );
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function getOptions() {
		$this->authorize( 'getOptions', Option::class );

		$model = Option::firstOrfail();

		return view( 'admin.edit', compact( 'model' ) );
	}

	/**
	 * Show the application dashboard.
	 *
	 * @param $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function postOptions( Requests\OptionRequest $request ) {
		// authorize the user ~/app/Policies/OptionPolicy.php
		$this->authorize( 'getOptions', Option::class );

		// retrieve the model (there should only ever be one row)
		$model = Option::firstOrfail();

		// update the model
		$model->update( $request->all() );

		// flash a success message
		flash()->success( 'Options updated successfully!' );

		// clear the app cache
		Artisan::call('cache:clear');

		// redirect to the edit page
		return redirect()->route( 'options.index' );
	}
}
