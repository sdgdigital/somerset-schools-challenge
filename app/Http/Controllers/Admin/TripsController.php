<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

use App\Trip;
use App\Weather;
use App\School;

class TripsController extends Controller
{
  protected $resource = 'trips';

  protected $weatherList;

  /**
   * Create a new controller instance.
   *
   */
  public function __construct()
  {
    view()->share('resource', $this->resource);

    $this->weatherList = Weather::selectList();
    view()->share('weatherList', $this->weatherList);
  }

  /**
   * Display a listing of the resource.
   *
   * @param $request
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    // authorize the user (pass in an empty model)
    $this->authorize('create', Trip::class);

    // if the user only has teacher rights
    // we only get their classroom, otherwise
    // we get all classrooms and trips for the school
    if (auth()->user()->cannot('super')) {

      $classroom = auth()->user()->classroom;

      $classrooms_list = [
        0 => $classroom->id,
      ];

    } else {

      // get the school
      $school = auth()->user()->school;

      // load the classrooms relation
      $classrooms_list = $school->classrooms()->get()->pluck('id')->toArray();

    }

    $trips = Trip::whereIn('classroom_id', $classrooms_list);

    // retrieve the collection
    $collection = $trips
      ->orderBy('date', 'desc')
      ->orderBy('classroom_id', 'asc')
      ->paginate();

    // return the view
    return view('admin.index', compact('collection'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    // authorize the user (pass in an empty model)
    $this->authorize('create', Trip::class);

    // set up the query
    $query = School::query();

    // add the relations
    $query = $query->with('classrooms');

    // restrict set
    $query = $query->where('id', '=', auth()->user()->school_id);

    // run the query
    $school = $query->firstOrFail();

    // return a list of classrooms to choose from
    $classroomList = pleaseSelect($school->classrooms->pluck('name', 'id')->toArray());

    // return the view
    return view('admin.create', compact('classroomList'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Requests\TripRequest $request)
  {
    // authorize the user (pass in an empty model)
    $this->authorize('create', Trip::class);

    // create the record
    $model = Trip::create($request->all());

    // flash a success message
    flash()->success(ucfirst(trans('app.trip')) . ' created successfully!');

    // redirect
    return redirect()->route($this->resource . '.edit', $model->id);
  }

  /**
   * Display the specified resource.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    return redirect()->route($this->resource . '.edit', $id);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    // retrieve the model
    $model = Trip::findOrFail($id);

    // authorize the user
    $this->authorize('update', $model);

    // set up the query
    $query = School::query();

    // add the relations
    $query = $query->with('classrooms');

    // restrict set
    $query = $query->where('id', '=', auth()->user()->school_id);

    // run the query
    $school = $query->firstOrFail();

    // return a list of classrooms to choose from
    $classroomList = pleaseSelect($school->classrooms->pluck('name', 'id')->toArray());

    // return the view
    return view('admin.edit', compact('model', 'classroomList'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  $request
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function update(Requests\TripRequest $request, $id)
  {
    // retrieve the model
    $model = Trip::findOrFail($id);

    // authorize the user
    $this->authorize('update', $model);

    // update the record
    $model->update($request->all());

    // flash a success message
    flash()->success(ucfirst(trans('app.trip')) . ' updated successfully!');

    // redirect
    return redirect()->route($this->resource . '.edit', $model->id);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    // retrieve the model
    $model = Trip::findOrFail($id);

    // authorize the user
    $this->authorize('delete', $model);

    // delete the model
    $model->delete();

    // flash a success message
    flash()->success(ucfirst(trans('app.trip')) . ' deleted successfully!');

    // redirect
    return redirect()->route($this->resource . '.index');
  }
}
