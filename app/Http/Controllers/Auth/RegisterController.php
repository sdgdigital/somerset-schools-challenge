<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Mail;

use App\User;
use App\LocalAuthority;
use App\Mail\AdminRegistered;

class RegisterController extends Controller
{
  /*
  |--------------------------------------------------------------------------
  | Register Controller
  |--------------------------------------------------------------------------
  |
  | This controller handles the registration of new users as well as their
  | validation and creation. By default this controller uses a trait to
  | provide this functionality without requiring any additional code.
  |
  */

  use RegistersUsers;

  /**
   * Where to redirect users after login / registration.
   *
   * @var string
   */
  protected $redirectTo = '/dashboard';

  /**
   * Create a new controller instance.
   *
   * @return mixed
   */
  public function __construct()
  {
    $this->middleware('guest');

    $localAuthorities = LocalAuthority::with('schools')->get();
    view()->share('localAuthorities', $localAuthorities);
  }

  /**
   * Get a validator for an incoming registration request.
   *
   * @param  array $data
   * @return mixed
   */
  protected function validator(array $data)
  {
    return Validator::make($data, [
      'email'        => 'required|email|max:255|unique:users',
      'password'     => 'required|min:6|confirmed',
      'first_name'   => 'required',
      'last_name'    => 'required',
      'display_name' => 'min:3|max:255|unique:users',
      'school_id'    => 'required|exists:schools,id',
      'accepted_terms'    => 'in:1',
      'privacy_statement' => 'in:1',
    ], [
      'school_id.exists' => 'We couldn\'t find that school',
      'accepted_terms.in'    => 'Please accept the terms & conditions to register.',
      'privacy_statement.in'    => 'Please confirm that you have read the privacy statement.',
    ]);
  }

  /**
   * Create a new user instance after a valid registration.
   *
   * @param  array $data
   * @return User
   */
  protected function create(array $data)
  {
    $model = User::create([
      'email'        => $data['email'],
      'password'     => bcrypt($data['password']),
      'first_name'   => $data['first_name'],
      'last_name'    => $data['last_name'],
      'display_name' => $data['display_name'],
      'school_id'    => $data['school_id'],
      'is_admin'     => 1,
    ]);

    return $model;
  }

  /**
   * The user has been registered.
   *
   * @param  $request
   * @param  mixed $user
   * @return mixed
   */
  protected function registered(Request $request, $user)
  {
    // send a confirmation email to the user
    Mail::to($request->user())->send(new AdminRegistered($user));
  }
}
