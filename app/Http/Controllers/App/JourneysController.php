<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Classroom;
use App\Trip;

class JourneysController extends Controller
{
    public function index(Request $request)
    {
        // retrieve the user
        $user = auth()->user()->load('classrooms');

        // retrieve or create a trip for today
        $trip = Trip::firstOrCreate([
            'date'         => app('today')->format(app('options')->date_format),
            'classroom_id' => $user->classrooms->first()->id,
        ]);

        // process the data if it was a POST
        if ($request->has('data')) {

            // retrieve the data
            $data = $request->input('data');

            // TODO add some validation

            // look for modes
            if (isset($data['modes'])) {

                // iterate over the modes data
                foreach ($data['modes'] as $key => $val) {

                    // assign the mode
                    $mode = $val['class_name'];

                    // assign the count
                    $trip->$mode = $val['counter'];
                }
            }

            // look for weather
            if (isset($data['weather_selected'])) {

                $trip->weather_id = $data['weather_selected'];

            }

            // save the trip
            $trip->save();
        }

        // build the travel data
        $travel_data = [];

        // reformat the data into our response
        foreach (app('modes') as $mode) {

            // retrieve the name from the mode
            $mode_name = $mode->class_name;

            // retrieve the qty from the trip
            $trip_mode_qty = empty($trip->$mode_name) ? 0 : $trip->$mode_name;

            // push it into the array
            $travel_data[$mode->id] = [
                'id'         => $mode->id,
                'name'       => $mode->name,
                'class_name' => $mode->class_name,
                'counter'    => $trip_mode_qty,
            ];
        }

        return response()->json([
            'success' => true,
            'data'    => [
                'weather_selected'          => $trip->weather_id,
                'pupils_in'                 => $trip->total_trips,
                'today'                     => app('today')->toIso8601String(),
                'classroom'                 => $user->classroom,
                'modes'                     => $travel_data,
                'weather'                   => app('weather'),
                'total_points'              => $trip->total_points,
                'total_sustainable_points'  => $trip->total_sustainable_points,
                'percent_sustainable_trips' => $trip->percent_sustainable_trips,
            ]
        ]);
    }

}