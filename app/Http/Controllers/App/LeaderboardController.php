<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\LocalAuthority;
use App\School;

class LeaderboardController extends Controller
{
    public function index(Request $request)
    {
        if(request()->input('public')==0){
            // get the user
            $user = auth()->user();

            $user_school = School::findOrFail(auth()->user()->school_id);

            // load the school relation
            $user->load('school');
        }else{
            $user_school = ['local_authority_id' => 1];
        }

        // prepare the modes data
        $modes = [];
        foreach (app('modes') as $mode) {
            $modes[$mode->id] = [
                'id'         => $mode->id,
                'name'       => $mode->name,
                'class_name' => $mode->class_name,
            ];
        }

        // get a count of the schools
        $schools_count = School::count();

        // prepare an array for the region data
        $regions = [
            0 => [
                'id'      => 0,
                'name'    => 'All regions',
                'schools' => $schools_count,
            ],
        ];

        $local_authorities = LocalAuthority::with('schools')->get();

        // prepare the modes data
        foreach ($local_authorities as $local_authority) {
            $regions[$local_authority->id] = [
                'id'      => $local_authority->id,
                'name'    => $local_authority->name,
                'schools' => $local_authority->schools->count(),
            ];
        }

        $top3 = [];

            $schools = School::inLocalAuthority()
                             ->orderBy('score', 'desc')
                             ->orderBy('name', 'asc')
                             ->get();

            $page  = 1;
            $count = 1;

            // add the positions
            $position    = 0;
            $prev_points = 0;

            $leaderboard = new \stdClass();

            $leaderboard->local_authority_selected = request()->input('local_authority');

            if (request()->input('local_authority') == 0) {
                $leaderboard->local_authority_name = 'All regions';
            } else {
                $leaderboard->local_authority_name = LocalAuthority::where('id', '=', request()->input('local_authority'))->first()->name;
            }

            $leaderboard->table = [];

            foreach ($schools as $key => $school) {

                if ($key == 0) {

                    $prev_points   = $school->score;
                    $school->pos   = 1;
                    $school->joint = false;
                    $position      = 1;

                } else {

                    if ($school->score == $prev_points) {
                        $school->pos   = $position;
                        $school->joint = ($count == 1) ? false : true;
                    } else {
                        $position++;
                        $school->pos   = $position;
                        $school->joint = false;
                        $prev_points   = $school->score;
                    }
                }

                if ($key < 3) {
                    $top3[$key] = $school;
                }
                $leaderboard->table[$page][$count] = $school;

                if (request()->input('public')==0 && $school->id == $user->school->id) {
                    $user->school->pos = $position;
                }

                if ($count == 10) {
                    $page++;
                    $count = 0;
                }

                $count++;
            }

        return response()->json([
            'success' => true,
            'data'    => [
                'school'      => $user_school,
                'modes'       => $modes,
                'regions'     => $regions,
                'top3'        => $top3,
                'leaderboard' => $leaderboard,
            ],
        ]);
    }

}
