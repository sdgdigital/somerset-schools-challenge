<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\LocalAuthority;
use App\School;
use App\Trip;

class CompareController extends Controller
{
  public function classroom(Request $request)
  {

    $classroom = auth()->user()->classroom;

    // prepare the modes data
    $modes = [];
    foreach (app('modes') as $mode) {
      $modes[$mode->id] = [
        'id'         => $mode->id,
        'name'       => $mode->name,
        'class_name' => $mode->class_name,
      ];
    }

    // get a count of the schools
    $schools_count = School::count();

    // prepare an array for the region data
    $regions_array = [
      0 => [
        'id'      => 0,
        'name'    => 'All regions',
        'schools' => $schools_count,
      ],
    ];

    $local_authorities = LocalAuthority::with('schools')->get();

    // prepare the regions data
    foreach ($local_authorities as $local_authority) {
      $regions_array[$local_authority->id] = [
        'id'      => $local_authority->id,
        'name'    => $local_authority->name,
        'schools' => $local_authority->schools->count(),
      ];
    }

    // prepare the weather data
    $weather_array = [];
    foreach (app('weather') as $w) {
      $weather_array[$w->id] = [
        'id'         => $w->id,
        'name'       => $w->name,
        'class_name' => $w->class_name,
      ];
    }

    // get all of the class trips
    $trips = Trip::where('classroom_id', '=', $classroom->id)
      ->whereNotNull('weather_id')
      ->orderBy('date', 'desc')
      ->get();

    $latest_trip = $trips->first();

    // prepare the trips data
    $trips_array = [];
    foreach ($trips as $trip) {
      $trips_array[$trip->date->format('Y-m-d')] = $trip;
    }

    // retrieve initial values
    $today = app('today');
    $start_date = app('competition')->start_date;

    // work out the number of weeks since the start of the competition
    $diffInWeeks = $start_date->diffInWeeks($today);

    // define an array to hold the weeks
    $weeks = [];

    // our working date is the start date
    $day = $start_date;

    // loop over the weeks
    for ($i = 0; $i <= $diffInWeeks; $i++) {

      // define an array to hold the days
      $week = [];

      // loop through mon - fri
      for ($j = 1; $j <= 5; $j++) {
        $week[] = [
          'formatted' => $day->format('l jS'),
          'date' => $day->format('Y-m-d'),
        ];

        $day->addDay();
      }

      // push the week into the weeks array
      $weeks[] = $week;

      // skip the weekend
      $day->addDays(2);
    }

    // reverse the order of the weeks
    $weeks = array_reverse($weeks);

    return response()->json([
      'success' => true,
      'data'    => [
        'today'       => app('today')->format('l j F'),
        'classroom'   => $classroom,
        'modes'       => $modes,
        'regions'     => $regions_array,
        'weather'     => $weather_array,
        'latest_trip' => empty($latest_trip) ? null : $latest_trip->date->format('Y-m-d'),
        'trips'       => $trips_array,
        'weeks'       => $weeks,
      ],
    ]);
  }
}
