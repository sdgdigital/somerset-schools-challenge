<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Page;

class CustomPageController extends Controller
{

  /**
   * Display a custom page.
   *
   * @param Request $request
   * @return \Illuminate\Http\Response
   */
  public function getPage(Request $request)
  {
    // retrieve the record
    $model = Page::where('url', '=', $request->getRequestUri())
      ->firstOrFail();

    return view('page', compact('model'));
  }

}
