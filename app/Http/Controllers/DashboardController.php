<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Carbon\Carbon;

class DashboardController extends Controller
{
  public function index()
  {
    $morning = Carbon::now()->hour < 12;

    return view('dashboard', compact('morning'));
  }
}
