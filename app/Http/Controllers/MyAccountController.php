<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;
use App\School;

class MyAccountController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function getAccount(Request $request)
  {
    $model = $request->user();

    $schoolList = School::selectList();

    return view('my-account', compact('model', 'schoolList'));
  }

  /**
   * Display a listing of the resource.
   *
   * @param Requests\MyAccountUpdate $request
   * @return \Illuminate\Http\Response
   */
  public function postAccount(Requests\MyAccountUpdate $request)
  {
    // retrieve the model
    $model = User::findOrFail($request->user()->id);

    // TODO if the captain and user are not in the same group

    // update the record
    $model->update($request->all());

    // flash a success message
    flash()->success('Account updated successfully!');

    // redirect
    return redirect()->route('my-account.edit');
  }

  public function getClassroom(Request $request) {

    $user = $request->user();
    $modes = \App\Mode::all();
    $classroom = \App\Classroom::findOrFail($request->user()->classroom_id);

    return response()->json([
      'success' => true,
      'data' => [
        'user' => $user,
        'modes' => $modes,
        'classroom' => $classroom,
      ],
      'status' => 200,
      'messages' => '',
      'errors' => '',
    ]);
  }

}
