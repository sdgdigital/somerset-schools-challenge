<?php

namespace App\Http\Requests;

class ModeRequest extends BaseFormRequest
{
  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    $excludes = '';
    if (ends_with($this->route()->getName(), 'update')) {
      $excludes = ',' . $this->route('mode');
    }

    return [
      'name'       => 'required|unique:modes,name' . $excludes,
      'class_name' => 'required',
      'points'     => 'required|integer',
    ];
  }
}
