<?php

namespace App\Http\Requests;

class TripRequest extends BaseFormRequest
{
    /**
     * Get data to be validated from the request.
     *
     * @return array
     */
    protected function validationData()
    {
        // retrieve the data
        $data = $this->all();

        // calculate the total
        $total = 0;
        foreach (app('modes') as $mode) {
            $total += (int)$data[$mode->class_name];
        }

        // append the attribute
        $data['total'] = $total;

        return $data;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $one_per_day = '';
        if (request()->route()->getName() != 'trips.update') {
            $one_per_day = '|custom_trip_qty';
        }

        return [
            'classroom_id' => 'required|exists:classrooms,id',
            'date'         => 'required|custom_trip_date' . $one_per_day,
            'walk'         => 'integer|min:0',
            'bus'          => 'integer|min:0',
            'bike'         => 'integer|min:0',
            'car'          => 'integer|min:0',
            'train'        => 'integer|min:0',
            'scooter'      => 'integer|min:0',
            'share'        => 'integer|min:0',
            'park'         => 'integer|min:0',
            'taxi'         => 'integer|min:0',
            'wheelchair'   => 'integer|min:0',
            'total'        => 'required|custom_classroom_pupils:classroom_id',
            //'average'      => 'required',
            'weather_id'   => 'required',
        ];
    }

    public function messages()
    {
        return [
            'classroom_id.required'         => 'Please select the classroom',
            'date.custom_trip_qty'          => 'You can only log one set of ' . trans('app.trips') . ' per day.',
            'weather_id.required'           => 'Please select the weather',
            'total.custom_classroom_pupils' => 'You have added too many trips',
        ];
    }
}
