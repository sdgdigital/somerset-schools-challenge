<?php

namespace App\Http\Requests;

class ClassroomRequest extends BaseFormRequest
{
  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    $excludes = '';
    if (ends_with($this->route()->getName(), 'update')) {
      $excludes = ',' . $this->route('classroom');
    }

    return [
      'name'             => 'required',
      'teacher_id'       => 'required|exists:users,id',
      'number_of_pupils' => 'required|integer',
      'year_group'       => 'required',
    ];
  }

  public function messages()
  {
    return [
      'teacher_id.required' => 'Please select a ' . trans('app.user') . '.',
      'teacher_id.exists'   => 'We could not find this ' . trans('app.user') . '.',
      'year_group.required' => 'Please select a year group.',
    ];
  }
}
