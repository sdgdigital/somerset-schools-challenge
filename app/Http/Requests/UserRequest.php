<?php

namespace App\Http\Requests;

class UserRequest extends BaseFormRequest
{
  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    $excludes = '';
    if (ends_with($this->route()->getName(), 'update')) {
      $excludes = ',' . $this->route('user');
    }

    return [
      'email'        => 'required|unique:users,email' . $excludes,
      'display_name' => 'sometimes|nullable|unique:users,display_name' . $excludes,
      'first_name'   => 'string|nullable',
      'last_name'    => 'string|nullable',
      'is_admin'     => 'sometimes|boolean',
      'is_super'     => 'sometimes|boolean',
    ];
  }
}
