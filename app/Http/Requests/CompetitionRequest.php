<?php

namespace App\Http\Requests;

class CompetitionRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'         => 'required',
            'description'  => '',
            'is_active'    => 'required|boolean',
            'start_date'   => 'required|date_format:' . app('options')->date_format,
            'end_date'     => 'required|date_format:' . app('options')->date_format . '|after:start_date',
            'grace_period' => 'required|date_format:' . app('options')->date_format . '|after:end_date',
            'duration'     => 'required|integer|min:1',
        ];
    }
}
