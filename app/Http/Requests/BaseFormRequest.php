<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;

class BaseFormRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Override the failed validation function to allow flash messages
   */
  protected function failedValidation(Validator $validator)
  {
    flash()->error('<strong>Warning!</strong> There were validation errors. ' . $validator->errors()->first());

    throw new ValidationException($validator, $this->response(
      $this->formatErrors($validator)
    ));
  }
}
