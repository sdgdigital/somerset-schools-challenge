<?php

namespace App\Http\Requests;

class MyAccountUpdate extends BaseFormRequest
{
  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'email'           => 'required|unique:users,email,' . $this->user()->id,
      'display_name'    => 'sometimes|unique:users,display_name,' . $this->user()->id . '|nullable',
      'first_name'      => 'string|nullable',
      'last_name'       => 'string|nullable',
    ];
  }
}
