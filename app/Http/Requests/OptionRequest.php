<?php

namespace App\Http\Requests;

class OptionRequest extends BaseFormRequest
{
  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'google_code'        => '',
      'date_format'        => 'required',
      'date_format_pretty' => 'required',
      'date_format_short'  => 'required',
      'distance_unit_abbr' => 'required',
      'weight_unit_abbr'   => 'required',
    ];
  }
}
