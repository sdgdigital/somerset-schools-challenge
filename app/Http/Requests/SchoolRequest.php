<?php

namespace App\Http\Requests;

class SchoolRequest extends BaseFormRequest
{
  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    $excludes = '';
    if (ends_with($this->route()->getName(), 'update')) {
      $excludes = ',' . $this->route('school');
    }

    return [
      'name'                                 => 'required|unique:schools,name' . $excludes,
      'description'                          => '',
      'manager_id'                           => '',
      'is_support'                           => '',
      'postcode'                             => '',
      'eastings'                             => '',
      'northings'                            => '',
      'local_authority_id'                   => '',
      'number_of_pupils'                     => '',
      'number_of_classes'                    => '',
      'phase_of_education'                   => '',
      'number_of_pt_stops_within_400_meters' => '',
      'pt_service_frequency_7am_to_830am'    => '',
      'rank_for_pt_stops'                    => '',
      'rank_for_frequency'                   => '',
      'combined_rank'                        => '',
      'overall_rank'                         => '',
      'accessibility_marker'                 => '',
    ];
  }
}
