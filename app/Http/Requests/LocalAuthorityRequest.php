<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;

class LocalAuthorityRequest extends BaseFormRequest
{
  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    $excludes = '';
    if (ends_with($this->route()->getName(), 'update')) {
      $excludes = ',' . $this->route('local_authority');
    }

    return [
      'name'        => 'required|unique:local_authorities,name' . $excludes,
      'description' => '',
      'is_support'  => '',
    ];
  }
}
