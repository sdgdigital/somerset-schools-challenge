<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mode extends Model
{
  use SoftDeletes;

  protected $fillable = [
    'name',
    'class_name',
    'points',
  ];

  protected $hidden = [
    'points',
    'created_at',
    'updated_at',
    'deleted_at'
  ];

  protected $appends = [
    'counter',
  ];

  //region Accessors

  public function getCounterAttribute()
  {
    return 0;
  }

  //endregion

  //region Mutators

  public function setNameAttribute($value)
  {
    $this->attributes['name'] = trim($value);
  }

  public function setClassAttribute($value)
  {
    $this->attributes['class_name'] = strtolower(trim($value));
  }

  public function setPointsAttribute($value)
  {
    $this->attributes['points'] = empty($value) ? 0 : $value;
  }

  //endregion
}
