<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Option extends Model
{
  use SoftDeletes;

  protected $fillable = [
    'date_format',
    'date_format_js',
    'date_format_pretty',
    'date_format_short',
    'distance_unit',
    'distance_unit_abbr',
    'weight_unit',
    'weight_unit_abbr',
    'google_code',
  ];

  public $dateFormatList = [
    'Y-m-d' => 'yyyy-mm-dd',
    'm-d-Y' => 'mm-dd-yyyy',
    'd-m-Y' => 'dd-mm-yyyy',
    'Y/m/d' => 'yyyy/mm/dd',
    'm/d/Y' => 'mm/dd/yyyy',
    'd/m/Y' => 'dd/mm/yyyy',
  ];

  public $weightUnitList = [
    'lb' => 'pound',
    'kg' => 'kilogram',
  ];

  public $distanceUnitList = [
    'mi' => 'mile',
    'km' => 'kilometer',
  ];

  //region Mutators

  public function setDateFormatAttribute($value)
  {
    $this->attributes['date_format_js'] = $this->dateFormatList[$value];
  }

  public function setDistanceUnitAttribute($value)
  {
    $this->attributes['distance_unit'] = $this->distanceUnitList[$value];
  }

  //endregion
}
