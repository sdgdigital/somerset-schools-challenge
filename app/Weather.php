<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Scopes\SortScope;

class Weather extends Model
{
  use SoftDeletes;

  protected $table = 'weather';

  protected $fillable = [
    'name',
    'class_name',
  ];

  protected $hidden = [
    'created_at',
    'updated_at',
    'deleted_at'
  ];

  protected $appends = [
    'selected',
  ];

  //region Accessors

  public function getSelectedAttribute()
  {
    return false;
  }

  //endregion

  //region Mutators

  public function setNameAttribute($value)
  {
    $this->attributes['name'] = trim($value);
  }

  public function setClassNameAttribute($value)
  {
    $this->attributes['class_name'] = trim($value);
  }

  //endregion


  //region Static

  public static function selectList()
  {
    // set up the query
    $query = Weather::query();

    // run the query
    $collection = $query
      ->get();

    // prepare the list
    $selectList = [];
    foreach ($collection as $model) {

      // build the string
      $selectList[$model->id] = $model->name;
    }

    // add the 'Please select' option
    $selectList = pleaseSelect($selectList);

    return $selectList;
  }

  //endregion
}
