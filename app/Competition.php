<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon\Carbon;

class Competition extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'description',
        'is_active',
        'start_date',
        'end_date',
        'grace_period',
        'duration',
    ];

    protected $dates = [
        'start_date',
        'end_date',
        'grace_period',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $casts = [
        'is_active' => 'boolean',
    ];

    //region Mutators

    public function setStartDateAttribute($value)
    {
        $this->attributes['start_date'] = Carbon::createFromFormat(app('options')->date_format, $value)->startOfDay();
    }

    public function setEndDateAttribute($value)
    {
        $this->attributes['end_date'] = Carbon::createFromFormat(app('options')->date_format, $value)->endOfDay();
    }

    public function setGracePeriodAttribute($value)
    {
        $this->attributes['grace_period'] = Carbon::createFromFormat(app('options')->date_format, $value)->endOfDay();
    }

    public function setDurationAttribute($value)
    {
        $this->attributes['duration'] = $value;
    }

    //endregion

}
