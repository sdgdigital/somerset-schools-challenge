<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
  use Notifiable;
  use SoftDeletes;

  protected $fillable = [
    'email',
    'school_id',
    'display_name',
    'first_name',
    'last_name',
    'is_admin',
    'is_super',
    'last_login',
    'last_login_ip',
    'password',
  ];

  protected $hidden = [
    'is_admin',
    'is_super',
    'last_login',
    'last_login_ip',
    'password',
    'remember_token',
    'created_at',
    'updated_at',
    'deleted_at'
  ];

  protected $dates = [
    'last_login',
    'created_at',
    'updated_at',
    'deleted_at'
  ];

  protected $casts = [
    'is_admin' => 'boolean',
    'is_super' => 'boolean',
  ];

  protected $appends = [
    'full_name',
  ];

  //region Events

//  protected static function boot()
//  {
//    parent::boot();
//
//    // set up new scope
//    $scope = new SortScope();
//    $self = new static;
//    $scope->attributes = $self->fillable;
//    static::addGlobalScope($scope);
//
//  }

  //endregion

  //region Relations

  public function classrooms()
  {
    return $this->belongsToMany(Classroom::class)->withTimestamps();
  }

  public function school()
  {
    return $this->belongsTo(School::class);
  }

  //endregion

  //region Authorization

  public function isAdmin()
  {
    return $this->is_admin;
  }

  public function isSuper()
  {
    return $this->is_super;
  }

  public function isTeacher()
  {
    return \DB::table('classroom_user')->select('user_id')->where('user_id', '=', auth()->user()->id)->count();
  }

//  public function owns($related)
//  {
//    return $this->id == $related->user_id;
//  }

  //endregion

  //region Accessors

  public function getClassroomAttribute()
  {
    return $this->classrooms->first();
  }

  public function getFullNameAttribute()
  {
    return $this->first_name . ' ' . $this->last_name;
  }

  public function getPrimaryNameAttribute()
  {

    // always return display name if present
    if (!empty($this->display_name)) {
      return $this->display_name;
    }

    // secondly, try the first name
    if (!empty($this->first_name)) {
      return $this->first_name;
    }

    // finally create a name using the users id
    return 'user' . $this->id;
  }

  //endregion

  //region Mutators

  public function setEmailAttribute($value)
  {
    $this->attributes['email'] = empty($value) ? null : strtolower(trim($value));
  }

  public function setDisplayNameAttribute($value)
  {
    $this->attributes['display_name'] = empty($value) ? null : trim($value);
  }

  public function setFirstNameAttribute($value)
  {
    $this->attributes['first_name'] = empty($value) ? null : trim($value);
  }

  public function setLastNameAttribute($value)
  {
    $this->attributes['last_name'] = empty($value) ? null : trim($value);
  }

  //endregion

  //region Static

  public static function selectList($school_id = null, $teacher_id = null)
  {
    // set up the query
    $query = User::query();

    // define the relation
    $relation = 'classrooms';

    // add the relation
    if (!empty($relation)) {
      $query = $query->with($relation);
    }

    // filter if necessary
    if (!empty($school_id)) {
      $query = $query->where('school_id', '=', $school_id);
    }

    // run the query
    $collection = $query
      ->orderBy('email', 'asc')
      ->get();

    // prepare the list
    $selectList = [];
    foreach ($collection as $model) {

      // default text
      //$assigned_text = '';

      // assigned text
      if (empty($model->classroom) || $model->id == $teacher_id) {
        //$assigned_text = ' - unassigned';
        // build the string
        //$selectList[$model->id] = $model->display_name . $assigned_text;
        $selectList[$model->id] = $model->first_name.' '.$model->last_name;
      }

    }

    // add the 'Please select' option
    $selectList = pleaseSelect($selectList);

    return $selectList;
  }

  //endregion

}
