<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Page extends Model
{
  use SoftDeletes;

  protected $fillable = [
    'name',
    'url',
    'body',
  ];

  //region Accessors

  public function getHtmlAttribute()
  {

    // create markdown parser
    $parser = new \cebe\markdown\Markdown();

    // parse the markdown and add it to the model
    return $parser->parse($this->body);
  }

  //endregion
}
