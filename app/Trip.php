<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon\Carbon;
use App\Events\TripSaved;

class Trip extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'competition_id',
        'classroom_id',
        'date',
        'walk',
        'bus',
        'bike',
        'car',
        'train',
        'scooter',
        'share',
        'park',
        'taxi',
        'wheelchair',
        'total_trips',
        'total_points',
        'total_sustainable_points',
        'percent_sustainable_trips',
        'weather_id',
    ];

    protected $dates = [
        'date',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $hidden = [
        'date',
        'walk',
        'bus',
        'bike',
        'car',
        'train',
        'scooter',
        'share',
        'park',
        'taxi',
        'wheelchair',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $appends = [
        'display_date',
        'modes',
        'most',
        'least',
    ];

    protected $casts = [
        'competition_id' => 'integer',
        'classroom_id'   => 'integer',
        'walk'           => 'integer',
        'bus'            => 'integer',
        'bike'           => 'integer',
        'car'            => 'integer',
        'train'          => 'integer',
        'scooter'        => 'integer',
        'share'          => 'integer',
        'park'           => 'integer',
        'taxi'           => 'integer',
        'wheelchair'     => 'integer',
        'total_trips'    => 'integer',
        'total_points'   => 'integer',
        'total_sustainable_points'   => 'integer',
        'weather_id'     => 'integer',
    ];

    //region Events

    public static function boot()
    {
        parent::boot();

        static::saving(function ($trip) {
            $trip->competition_id            = app('competition')->id;
            $trip->total_trips               = 0;
            $trip->total_points              = 0;
            $trip->total_sustainable_points  = 0;
            $trip->percent_sustainable_trips = 0;
        });

        // automatically trigger an update on the classroom relation
        static::updated(function ($trip) {
            event(new TripSaved($trip));
        });
    }

    //endregion

    //region Accessors

    public function getWeatherAttribute()
    {
        return $this->weather_id;
    }

    public function getDisplayDateAttribute()
    {
        $today = "";
        if (app('today') == $this->date) {
            $today = "Today, ";
        }

        return [
            'day_of_week' => $this->date->format('l'),
            'day'         => $this->date->format('j'),
            'month'       => $this->date->format('F'),
            'year'        => $this->date->format('Y'),
            'ordinal'     => $this->date->format('S'),
            'iso'         => $this->date->toIso8601String(),
            'today'       => $today,
        ];
    }

    // TODO is this required?
    public function getModesAttribute()
    {
        $array = [];
        foreach (app('modes') as $mode) {
            $array[$mode->id] = $this->{$mode->class_name};
        }

        return $array;
    }

    public function getMostAttribute()
    {
        $array = [];
        foreach (app('modes') as $mode) {
            $array[$mode->id] = $this->{$mode->class_name};
        }

        // sort the values
        arsort($array);

        // get the key for the top mode
        $key = array_keys($array)[0];

        return $key;
    }

    public function getLeastAttribute()
    {
        $array = [];
        foreach (app('modes') as $mode) {
            $array[$mode->id] = $this->{$mode->class_name};
        }

        // sort the values
        asort($array);

        // get the key for the top mode
        $key = array_keys($array)[0];

        return $key;
    }

    //endregion

    //region Mutators

    public function setWalkAttribute($value)
    {
        $this->attributes['walk'] = $this->setToZeroIfEmpty($value);
    }

    public function setBusAttribute($value)
    {
        $this->attributes['bus'] = $this->setToZeroIfEmpty($value);
    }

    public function setBikeAttribute($value)
    {
        $this->attributes['bike'] = $this->setToZeroIfEmpty($value);
    }

    public function setCarAttribute($value)
    {
        $this->attributes['car'] = $this->setToZeroIfEmpty($value);
    }

    public function setTrainAttribute($value)
    {
        $this->attributes['train'] = $this->setToZeroIfEmpty($value);
    }

    public function setScooterAttribute($value)
    {
        $this->attributes['scooter'] = $this->setToZeroIfEmpty($value);
    }

    public function setShareAttribute($value)
    {
        $this->attributes['share'] = $this->setToZeroIfEmpty($value);
    }

    public function setParkAttribute($value)
    {
        $this->attributes['park'] = $this->setToZeroIfEmpty($value);
    }

    public function setTaxiAttribute($value)
    {
        $this->attributes['taxi'] = $this->setToZeroIfEmpty($value);
    }

    public function setWheelchairAttribute($value)
    {
        $this->attributes['wheelchair'] = $this->setToZeroIfEmpty($value);
    }

    public function setDateAttribute($value)
    {
        $this->attributes['date'] = Carbon::createFromFormat(app('options')->date_format, $value);
    }

    public function setWeatherIdAttribute($value)
    {
        $this->attributes['weather_id'] = empty($value) ? null : $value;
    }

    public function setTotalTripsAttribute($value = 0)
    {
        // tally the trips for each mode
        foreach (app('modes') as $mode) {
            $value += $this->{$mode->class_name};
        }

        // set the attribute
        $this->attributes['total_trips'] = $value;
    }

    public function setTotalPointsAttribute($value)
    {
        // tally the points for each mode
        foreach (app('modes') as $mode) {
            $value += $this->{$mode->class_name} * $mode->points;
        }

        // set the attribute
        $this->attributes['total_points'] = $value;
    }

    public function setTotalSustainablePointsAttribute($value)
    {
        // tally the points for each mode
        foreach (app('modes') as $mode) {
            if($mode->class_name == 'walk' || $mode->class_name == 'bike' || $mode->class_name == 'scooter' || $mode->class_name == 'park' || $mode->class_name == 'wheelchair'){
                $value += $this->{$mode->class_name} * $mode->points;
            }
        }

        // set the attribute
        //app('debugbar')->error('value: '.$value);
        $this->attributes['total_sustainable_points'] = $value;
    }

    public function setPercentSustainableTripsAttribute($value = 0)
    {
        // sustainable modes: walk, bike, scooter, park, wheelchair
        $total_sustainable_trips = $this->walk + $this->bike + $this->scooter + $this->park + $this->wheelchair;

        // catch division by zero
        if ($total_sustainable_trips != 0) {
            // calculate and format to 1 decimal place
            $value = number_format($total_sustainable_trips / $this->total_trips * 100, 1);
        }

        // set the attribute
        $this->attributes['percent_sustainable_trips'] = $value;
    }

    //endregion

    //region Relations

    public function competition()
    {
        return $this->belongsTo(Competition::class);
    }

    public function classroom()
    {
        return $this->belongsTo(Classroom::class);
    }

    //endregion

    //region Scopes

    public function scopeInCompetition($query)
    {
        return $query->where('date', '>=', app('competition')->start_date)->where('date', '<=', app('competition')->end_date);
    }

    //endregion

    private function setToZeroIfEmpty($value)
    {
        return empty($value) ? 0 : $value;
    }

}
