<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use App\Trip;

class TripSaved
{
  use InteractsWithSockets, SerializesModels;

  public $trip;

  /**
   * Create a new event instance.
   *
   * @param Trip $trip
   */
  public function __construct(Trip $trip)
  {
    $this->trip = $trip;
  }

  /**
   * Get the channels the event should broadcast on.
   *
   * @return Channel|array
   */
  public function broadcastOn()
  {
    return new PrivateChannel('channel-data');
  }
}
