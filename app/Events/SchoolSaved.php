<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use App\School;

class SchoolSaved
{
  use InteractsWithSockets, SerializesModels;

  public $school;

  /**
   * Create a new event instance.
   *
   * @param School $school
   */
  public function __construct(School $school)
  {
    $this->school = $school;
  }

  /**
   * Get the channels the event should broadcast on.
   *
   * @return Channel|array
   */
  public function broadcastOn()
  {
    return new PrivateChannel('channel-data');
  }
}
