<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use App\Classroom;

class ClassroomSaved
{
  use InteractsWithSockets, SerializesModels;

  public $classroom;

  /**
   * Create a new event instance.
   *
   * @param Classroom $classroom
   */
  public function __construct(Classroom $classroom)
  {
    $this->classroom = $classroom;
  }

  /**
   * Get the channels the event should broadcast on.
   *
   * @return Channel|array
   */
  public function broadcastOn()
  {
    return new PrivateChannel('channel-data');
  }
}
