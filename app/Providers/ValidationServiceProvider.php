<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use Validator;
use Carbon\Carbon;
use App\Trip;
use App\Classroom;

class ValidationServiceProvider extends ServiceProvider
{
  /**
   * Bootstrap the application services.
   *
   * @return void
   */
  public function boot()
  {
    Validator::extend('custom_classroom_pupils', function ($attribute, $value, $parameters, $validator) {

      // get the data
      $data = $validator->getData();

      if (empty($data['classroom_id'])) {
        return false;
      }

      // retrieve the number of pupils
      $result = \DB::table('classrooms')->select('number_of_pupils')->where('id', $data['classroom_id'])->first();

      // make the comparison
      return $value <= $result->number_of_pupils;
    });

    Validator::extend('custom_trip_qty', function ($attribute, $value, $parameters, $validator) {

      // convert the date to ISO format
      $date = Carbon::createFromFormat(app('options')->date_format, $value);

      // count the number of logged trips
      $count = Trip::where('date', '=', $date->format('Y-m-d'))
        ->where('classroom_id', '=', request()->input('classroom_id'))
        ->count();

      return $count < 1;
    });

    Validator::extend('custom_trip_date', function ($attribute, $value, $parameters, $validator) {
      $date = Carbon::createFromFormat(app('options')->date_format, $value);
      return app('tomorrow')->gt($date);
    });

    Validator::extend('custom_admin_only', function ($attribute, $value, $parameters, $validator) {
      if (!auth()->user()->can('admin')) {
        abort(403);
      }
      return true;
    });
  }

  /**
   * Register the application services.
   *
   * @return void
   */
  public function register()
  {
    //
  }
}
