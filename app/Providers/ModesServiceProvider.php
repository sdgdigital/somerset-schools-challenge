<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use Cache;
use App\Mode;

class ModesServiceProvider extends ServiceProvider
{
  /**
   * Bootstrap the application services.
   *
   * @return void
   */
  public function boot()
  {
    // share a singleton with the entire app
    app()->singleton('modes', function () {

      // retrieve or set the cache
      return Cache::remember('modes', config('app.cache_duration'), function () {
        return Mode::all();
      });
    });
  }

  /**
   * Register the application services.
   *
   * @return void
   */
  public function register()
  {
    //
  }
}
