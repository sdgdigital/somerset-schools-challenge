<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

use App\Option;
use App\Policies\OptionPolicy;
use App\Mode;
use App\Policies\ModePolicy;
use App\Weather;
use App\Policies\WeatherPolicy;
use App\Competition;
use App\Policies\CompetitionPolicy;
use App\LocalAuthority;
use App\Policies\LocalAuthorityPolicy;
use App\School;
use App\Policies\SchoolPolicy;
use App\Classroom;
use App\Policies\ClassroomPolicy;
use App\User;
use App\Policies\UserPolicy;
use App\Page;
use App\Policies\PagePolicy;
use App\Trip;
use App\Policies\TripPolicy;

class AuthServiceProvider extends ServiceProvider
{
  /**
   * The policy mappings for the application.
   *
   * @var array
   */
  protected $policies = [
    Option::class         => OptionPolicy::class,
    Competition::class    => CompetitionPolicy::class,
    Mode::class           => ModePolicy::class,
    Weather::class        => WeatherPolicy::class,
    LocalAuthority::class => LocalAuthorityPolicy::class,
    School::class         => SchoolPolicy::class,
    Classroom::class      => ClassroomPolicy::class,
    User::class           => UserPolicy::class,
    Page::class           => PagePolicy::class,
    Trip::class           => TripPolicy::class,
  ];

  /**
   * Register any authentication / authorization services.
   *
   * @return void
   */
  public function boot()
  {
    // test for the user_id override in the .env
    if (!app()->runningInConsole() && config('app.auto_login_id') != null) {
      auth()->loginUsingId(config('app.auto_login_id'));
    }

    app()->singleton('user', function () {
      return auth()->user();
    });

    $this->registerPolicies();

    Gate::define('super', function ($user) {
      return $user->isSuper();
    });

    Gate::define('admin', function ($user) {
      return $user->isSuper() || $user->isAdmin();
    });

    Gate::define('teach', function ($user) {
      return $user->isSuper() || $user->isAdmin() || $user->isTeacher();
    });

  }
}
