<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\ViewComposers\MasterViewComposer;

class ViewComposerServiceProvider extends ServiceProvider
{
  /**
   * Bootstrap the application services.
   *
   * @return void
   */
  public function boot()
  {
    view()->composer('*', MasterViewComposer::class);
  }

  /**
   * Register the application services.
   *
   * @return void
   */
  public function register()
  {
    //
  }
}
