<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use Carbon\Carbon;

class DateServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

      app()->singleton('now', function () {
        return Carbon::now();
      });

      app()->singleton('today', function () {
        return Carbon::today();
      });

      app()->singleton('tomorrow', function () {
        return Carbon::tomorrow();
      });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
