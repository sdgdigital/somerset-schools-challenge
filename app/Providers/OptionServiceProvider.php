<?php

namespace App\Providers;

use App\Option;
use Cache;
use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;

class OptionServiceProvider extends ServiceProvider
{
  /**
   * Bootstrap the application services.
   *
   * @return void
   */
  public function boot()
  {
    // a global singleton to hold the system options object
    app()->singleton('options', function () {

      // retrieve or set the cache (24 hours)
      return Cache::remember('options', config('app.cache_duration'), function () {
        return Option::firstOrFail();
      });
    });
  }

  /**
   * Register the application services.
   *
   * @return void
   */
  public function register()
  {
    //
  }
}
