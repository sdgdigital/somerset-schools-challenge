<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use Form;

class FormServiceProvider extends ServiceProvider
{
  /**
   * Bootstrap the application services.
   *
   * @return void
   */
  public function boot()
  {
    Form::component('bsLabel', 'admin.components.form-label', ['name', 'label']);
    Form::component('bsText', 'admin.components.form-text', ['name', 'label', 'value' => null, 'attributes' => []]);
    Form::component('bsEmail', 'admin.components.form-email', ['name', 'label', 'value' => null, 'attributes' => []]);
    Form::component('bsNumber', 'admin.components.form-number', ['name', 'label', 'value' => null, 'attributes' => []]);
    Form::component('bsNumberCustom', 'admin.components.form-number-col-xs-2', ['name', 'label', 'value' => null, 'attributes' => []]);
    Form::component('bsPassword', 'admin.components.form-password', ['name', 'label', 'attributes' => []]);
    Form::component('bsTextArea', 'admin.components.form-textarea', ['name', 'label', 'value' => null, 'attributes' => []]);
    Form::component('bsCheckbox', 'admin.components.form-checkbox', ['name', 'label', 'checked' => false]);
    Form::component('bsDate', 'admin.components.form-date', ['name', 'label', 'value' => null, 'attributes' => []]);
    Form::component('bsSelect', 'admin.components.form-select', ['name', 'label', 'value' => null, 'list' => [], 'attributes' => []]);
    Form::component('bsImage', 'admin.components.form-image', ['name', 'label', 'value' => null, 'attributes' => []]);
    Form::component('bsSubmit', 'admin.components.form-submit', ['type' => null, 'attributes' => []]);
    Form::component('bsButtonCreate', 'admin.components.form-button-create', []);
    Form::component('bsButtonUpdate', 'admin.components.form-button-update', []);
    Form::component('bsButtonCancel', 'admin.components.form-button-cancel', []);
    Form::component('bsSubmit', 'admin.components.form-submit', []);
  }

  /**
   * Register the application services.
   *
   * @return void
   */
  public function register()
  {
    //
  }
}
