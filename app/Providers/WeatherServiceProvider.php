<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use Cache;
use App\Weather;

class WeatherServiceProvider extends ServiceProvider
{
  /**
   * Bootstrap the application services.
   *
   * @return void
   */
  public function boot()
  {
    // share a singleton with the entire app
    app()->singleton('weather', function () {

      // retrieve or set the cache
      return Cache::remember('weather', config('app.cache_duration'), function () {
        return Weather::all();
      });
    });
  }

  /**
   * Register the application services.
   *
   * @return void
   */
  public function register()
  {
    //
  }
}
