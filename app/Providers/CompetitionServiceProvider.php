<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use Cache;
use App\Competition;

class CompetitionServiceProvider extends ServiceProvider
{
  /**
   * Bootstrap the application services.
   *
   * @return void
   */
  public function boot()
  {
    // share a singleton with the entire app
    app()->singleton('competition', function () {

      // retrieve or set the cache
      return Cache::remember('competition', config('app.cache_duration'), function () {
        return Competition::where('is_active', '=', 1)->first();
      });
    });
  }

  /**
   * Register the application services.
   *
   * @return void
   */
  public function register()
  {
    //
  }
}
