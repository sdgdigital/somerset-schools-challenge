<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\School;

class RecalculateSchools extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'recalculate:schools';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Recalculate schools';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $schools = School::all();
      foreach ($schools as $school) {
        $school->save();
      }
    }
}
