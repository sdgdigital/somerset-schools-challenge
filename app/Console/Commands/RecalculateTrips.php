<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Trip;

class RecalculateTrips extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'recalculate:trips';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Recalculate trips';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
    $trips = Trip::all();
    foreach ($trips as $trip) {
      $trip->save();
    }
  }
}
