<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Classroom;

class RecalculateClassrooms extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'recalculate:classrooms';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Recalculate classrooms';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
    $classrooms = Classroom::all();
    foreach ($classrooms as $classroom) {
      $classroom->save();
    }
  }
}
