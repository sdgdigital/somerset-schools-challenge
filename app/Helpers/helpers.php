<?php

function mergeCssAttributes($attributes, $defaultClasses)
{
  if (!empty($defaultClasses)) {
    if (!empty($attributes['class'])) {
      $parts = explode(' ', $attributes['class']);
      array_unshift($parts, $defaultClasses);
      $attributes['class'] = implode(' ', $parts);
    } else {
      $attributes['class'] = $defaultClasses;
    }
  }
  return $attributes;
}

function getResourceAction()
{
  $parts = explode('.', request()->route()->getName());
  return $parts[1];
}

function formatNumber($num)
{

  $x = round($num);

  if ($x < 1000) {
    return $num;
  }

  $x_number_format = number_format($x);
  $x_array = explode(',', $x_number_format);
  $x_parts = ['k', 'm', 'b', 't'];
  $x_count_parts = count($x_array) - 1;
  $x_display = $x;
  $x_display = $x_array[0] . ((int)$x_array[1][0] !== 0 ? '.' . $x_array[1][0] : '');
  $x_display .= $x_parts[$x_count_parts - 1];

  return $x_display;
}

function formatPosition($key = 0, $perPage = 1, $currPage = 1)
{
  return ($key + 1) + ($perPage * ($currPage - 1));
}

function formatOrdinal($key) {
  switch ($key) {
    case 11:
    case 12:
    case 13:
      return 'th';
      break;

    case ($key % 10 == 1):
      return 'st';
      break;

    case ($key % 10 == 2):
      return 'nd';
      break;

    case ($key % 10 == 3):
      return 'rd';
      break;

    case 0:
      return '';
      break;

    default:
      return 'th';
      break;
  }
}

function pleaseSelect($arr = [], $text = '-- Please select --') {
  return [null => $text] + $arr;
}

function randomPassword() {
  $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
  $pass = array(); //remember to declare $pass as an array
  $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
  for ($i = 0; $i < 12; $i++) {
    $n = rand(0, $alphaLength);
    $pass[] = $alphabet[$n];
  }
  return implode($pass); //turn the array into a string
}