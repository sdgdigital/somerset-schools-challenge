<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

use App\User;
use App\LocalAuthority;

class LocalAuthorityPolicy
{
  use HandlesAuthorization;

  /**
   * Determine whether the user can view the local authority.
   *
   * @param  User $user
   * @param  $ability
   * @return mixed
   */
  public function before(User $user, $ability)
  {
    if ($user->can('super')) {
      return true;
    }
  }

  /**
   * Determine whether the user can view the local authority.
   *
   * @param  User $user
   * @param  LocalAuthority $local_authority
   * @return mixed
   */
  public function view(User $user, LocalAuthority $local_authority)
  {
    //
  }

  /**
   * Determine whether the user can create local authorities.
   *
   * @param  User $user
   * @return mixed
   */
  public function create(User $user)
  {
    //
  }

  /**
   * Determine whether the user can update the local authority.
   *
   * @param  User $user
   * @param  LocalAuthority $local_authority
   * @return mixed
   */
  public function update(User $user, LocalAuthority $local_authority)
  {
    //
  }

  /**
   * Determine whether the user can delete the local authority.
   *
   * @param  User $user
   * @param  LocalAuthority $local_authority
   * @return mixed
   */
  public function delete(User $user, LocalAuthority $local_authority)
  {
    //
  }
}
