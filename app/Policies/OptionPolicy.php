<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

use App\User;

class OptionPolicy
{
  use HandlesAuthorization;

  /**
   * Determine whether the user can view the option.
   *
   * @param  User $user
   * @param  $ability
   * @return mixed
   */
  public function before(User $user, $ability)
  {
    // only super has the rights to view these pages
    if ($user->can('super')) {
      return true;
    }
  }

  /**
   * Determine whether the user can view the option.
   *
   * @param  User $user
   * @return mixed
   */
  public function getOptions(User $user)
  {
    // this is taken care of in the __construct function in Option::class
  }

  /**
   * Determine whether the user can create options.
   *
   * @param  User $user
   * @return mixed
   */
  public function postOptions(User $user)
  {
    // this is taken care of in the __construct function in Option::class
  }

}
