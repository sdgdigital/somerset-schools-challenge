<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

use App\User;
use App\Trip;

class TripPolicy
{
  use HandlesAuthorization;

  /**
   * @param User $user
   * @param $ability
   * @return mixed
   */
  public function before(User $user, $ability)
  {
    return true;
  }

  /**
   * Determine whether the user can view the school.
   *
   * @param  User $user
   * @param  Trip $trip
   * @return mixed
   */
  public function view(User $user, Trip $trip)
  {
    //
  }

  /**
   * Determine whether the user can create schools.
   *
   * @param  User $user
   * @return mixed
   */
  public function create(User $user)
  {
    //
  }

  /**
   * Determine whether the user can update the school.
   *
   * @param  User $user
   * @param  Trip $trip
   * @return mixed
   */
  public function update(User $user, Trip $trip)
  {
    //
  }

  /**
   * Determine whether the user can delete the school.
   *
   * @param  User $user
   * @param  Trip $trip
   * @return mixed
   */
  public function delete(User $user, Trip $trip)
  {
    //
  }

}
