<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

use App\User;
use App\School;

class SchoolPolicy
{
  use HandlesAuthorization;

  /**
   * Determine whether the user can view the school.
   *
   * @param  User $user
   * @param  $ability
   * @return mixed
   */
  public function before(User $user, $ability)
  {
    if ($user->can('super')) {
      return true;
    }
  }

  /**
   * Determine whether the user can view the school.
   *
   * @param  User $user
   * @param  School $school
   * @return mixed
   */
  public function view(User $user, School $school)
  {
    //
  }

  /**
   * Determine whether the user can create schools.
   *
   * @param  User $user
   * @return mixed
   */
  public function create(User $user)
  {
    //
  }

  /**
   * Determine whether the user can update the school.
   *
   * @param  User $user
   * @param  School $school
   * @return mixed
   */
  public function update(User $user, School $school)
  {
    //
  }

  /**
   * Determine whether the user can delete the school.
   *
   * @param  User $user
   * @param  School $school
   * @return mixed
   */
  public function delete(User $user, School $school)
  {
    //
  }
}
