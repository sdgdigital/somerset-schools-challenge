<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

use App\User;
use App\Classroom;

class ClassroomPolicy
{
  use HandlesAuthorization;

  /**
   * Determine whether the user can view the classroom.
   *
   * @param  User $user
   * @param  $ability
   * @return mixed
   */
  public function before(User $user, $ability)
  {
    if ($user->can('admin')) {
      return true;
    }
  }

  /**
   * Determine whether the user can view the classroom.
   *
   * @param  User $user
   * @param  Classroom $classroom
   * @return mixed
   */
  public function view(User $user, Classroom $classroom)
  {
    //
  }

  /**
   * Determine whether the user can create classrooms.
   *
   * @param  User $user
   * @return mixed
   */
  public function create(User $user)
  {
    //
  }

  /**
   * Determine whether the user can update the classroom.
   *
   * @param  User $user
   * @param  Classroom $classroom
   * @return mixed
   */
  public function update(User $user, Classroom $classroom)
  {
    //
  }

  /**
   * Determine whether the user can delete the classroom.
   *
   * @param  User $user
   * @param  Classroom $classroom
   * @return mixed
   */
  public function delete(User $user, Classroom $classroom)
  {
    //
  }
}
