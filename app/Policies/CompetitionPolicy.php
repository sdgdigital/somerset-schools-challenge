<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

use App\User;
use App\Competition;

class CompetitionPolicy
{
  use HandlesAuthorization;

  /**
   * @param  User $user
   * @param  $ability
   * @return mixed
   */
  public function before(User $user, $ability)
  {
    if ($user->can('super')) {
      return true;
    }
  }

  /**
   * Determine whether the user can view the competition.
   *
   * @param  User $user
   * @param  Competition $competition
   * @return mixed
   */
  public function view(User $user, Competition $competition)
  {
    //
  }

  /**
   * Determine whether the user can create competitions.
   *
   * @param  User $user
   * @return mixed
   */
  public function create(User $user)
  {
    //
  }

  /**
   * Determine whether the user can update the competition.
   *
   * @param  User $user
   * @param  Competition $competition
   * @return mixed
   */
  public function update(User $user, Competition $competition)
  {
    //
  }

  /**
   * Determine whether the user can delete the competition.
   *
   * @param  User $user
   * @param  Competition $competition
   * @return mixed
   */
  public function delete(User $user, Competition $competition)
  {
    //
  }
}
