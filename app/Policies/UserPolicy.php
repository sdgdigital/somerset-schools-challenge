<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

use App\User;

class UserPolicy
{
  use HandlesAuthorization;

  /**
   * Determine whether the user can view the user.
   *
   * @param  User $user
   * @param  $ability
   * @return mixed
   */
  public function before(User $user, $ability)
  {
    if ($user->can('admin')) {
      return true;
    }
  }

  /**
   * Determine whether the user can view the user.
   *
   * @param  User $user
   * @param  User $user_model
   * @return mixed
   */
  public function view(User $user, User $user_model)
  {
    //
  }

  /**
   * Determine whether the user can create users.
   *
   * @param  User $user
   * @return mixed
   */
  public function create(User $user)
  {
    //
  }

  /**
   * Determine whether the user can update the user.
   *
   * @param  User $user
   * @param  User $user_model
   * @return mixed
   */
  public function update(User $user, User $user_model)
  {
    //
  }

  /**
   * Determine whether the user can delete the user.
   *
   * @param  User $user
   * @param  User $user_model
   * @return mixed
   */
  public function delete(User $user, User $user_model)
  {
    //
  }
}
