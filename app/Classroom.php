<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Events\ClassroomSaved;

class Classroom extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'school_id',
        'year_group',
        'number_of_pupils',
        'walk',
        'bus',
        'bike',
        'car',
        'train',
        'scooter',
        'share',
        'park',
        'taxi',
        'wheelchair',
        'total_trips',
        'total_points',
        'total_sustainable_points',
        'percent_sustainable_trips',
        'participation',
    ];

    protected $hidden = [
        'walk',
        'bus',
        'bike',
        'car',
        'train',
        'scooter',
        'share',
        'park',
        'taxi',
        'wheelchair',
        'total_trips',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $appends = [
        'modes',
        'most',
        'least',
        'total_trips',
    ];

    public $yearGroup = [
        'nursery'   => 'Nursery',
        'reception' => 'Reception',
        'year-1'    => 'Year 1',
        'year-2'    => 'Year 2',
        'year-3'    => 'Year 3',
        'year-4'    => 'Year 4',
        'year-5'    => 'Year 5',
        'year-6'    => 'Year 6',
        'year-7'    => 'Year 7',
        'year-8'    => 'Year 8',
        'year-9'    => 'Year 9',
        'year-10'   => 'Year 10',
        'year-11'   => 'Year 11',
        'year-12'   => 'Year 12',
        'year-13'   => 'Year 13',
        'year-14'   => 'Year 14',
    ];

    //region Events

    public static function boot()
    {
        parent::boot();

        static::saving(function ($classroom) {
            $classroom->walk                      = 0;
            $classroom->bus                       = 0;
            $classroom->bike                      = 0;
            $classroom->car                       = 0;
            $classroom->train                     = 0;
            $classroom->scooter                   = 0;
            $classroom->share                     = 0;
            $classroom->park                      = 0;
            $classroom->taxi                      = 0;
            $classroom->wheelchair                = 0;
            $classroom->total_trips               = 0;
            $classroom->total_points              = 0;
            $classroom->total_sustainable_points  = 0;
            $classroom->percent_sustainable_trips = 0;
            $classroom->percent_participation     = 0;
        });

        static::updated(function ($classroom) {
            event(new ClassroomSaved($classroom));
        });
    }

    //endregion

    //region Relations

    public function teachers()
    {
        return $this->belongsToMany(User::class)->withTimestamps();
    }

    public function trips()
    {
        return $this->hasMany(Trip::class);
    }

    //endregion

    //region Accessors

    public function getTeacherAttribute()
    {
        return $this->teachers->first();
    }

    // TODO this isn't right
//    public function getAverageAttribute()
//    {
//        // sustainable modes (walk, bike, scooter)
//        $sustainable_trips = $this->walk + $this->bike + $this->scooter;
//
//        // catch division by zero
//        $average = 0;
//        if ($sustainable_trips != 0) {
//            $average = $sustainable_trips / $this->total_trips * 100;
//        }
//
//        // write the value
//        $this->attributes['average'] = number_format($average, 1);
//    }

    // TODO is the required?
    public function getModesAttribute()
    {
        $array = [];
        foreach (app('modes') as $mode) {
            $array[$mode->id] = $this->{$mode->class_name};
        }

        return $array;
    }

    public function getMostAttribute()
    {
        $array = [];
        foreach (app('modes') as $mode) {
            $array[$mode->id] = $this->{$mode->class_name};
        }

        // sort the values
        arsort($array);

        // get the key for the top mode
        $key = array_keys($array)[0];

        return $key;
    }

    public function getLeastAttribute()
    {
        $array = [];
        foreach (app('modes') as $mode) {
            $array[$mode->id] = $this->{$mode->class_name};
        }

        // sort the values
        asort($array);

        // get the key for the top mode
        $key = array_keys($array)[0];

        return $key;
    }

    //endregion

    //region Mutators

    public function setWalkAttribute()
    {
        $mode       = 'walk';
        $trip_count = $this->trips()
                           ->inCompetition()
                           ->sum($mode);

        $this->attributes[$mode] = $trip_count;
    }

    public function setBusAttribute()
    {
        $mode       = 'bus';
        $trip_count = $this->trips()
                           ->inCompetition()
                           ->sum($mode);

        $this->attributes[$mode] = $trip_count;
    }

    public function setBikeAttribute()
    {
        $mode       = 'bike';
        $trip_count = $this->trips()
                           ->inCompetition()
                           ->sum($mode);

        $this->attributes[$mode] = $trip_count;
    }

    public function setCarAttribute()
    {
        $mode       = 'car';
        $trip_count = $this->trips()
                           ->inCompetition()
                           ->sum($mode);

        $this->attributes[$mode] = $trip_count;
    }

    public function setTrainAttribute()
    {
        $mode       = 'train';
        $trip_count = $this->trips()
                           ->inCompetition()
                           ->sum($mode);

        $this->attributes[$mode] = $trip_count;
    }

    public function setScooterAttribute()
    {
        $mode       = 'scooter';
        $trip_count = $this->trips()
                           ->inCompetition()
                           ->sum($mode);

        $this->attributes[$mode] = $trip_count;
    }

    public function setShareAttribute()
    {
        $mode       = 'share';
        $trip_count = $this->trips()
                           ->inCompetition()
                           ->sum($mode);

        $this->attributes[$mode] = $trip_count;
    }

    public function setParkAttribute()
    {
        $mode       = 'park';
        $trip_count = $this->trips()
                           ->inCompetition()
                           ->sum($mode);

        $this->attributes[$mode] = $trip_count;
    }

    public function setTaxiAttribute()
    {
        $mode       = 'taxi';
        $trip_count = $this->trips()
                           ->inCompetition()
                           ->sum($mode);

        $this->attributes[$mode] = $trip_count;
    }

    public function setWheelchairAttribute()
    {
        $mode       = 'wheelchair';
        $trip_count = $this->trips()
                           ->inCompetition()
                           ->sum($mode);

        $this->attributes[$mode] = $trip_count;
    }

    public function setTotalTripsAttribute()
    {
        $trip_count = $this->trips()
                           ->inCompetition()
                           ->sum('total_trips');

        $this->attributes['total_trips'] = $trip_count;
    }

    public function setTotalPointsAttribute()
    {
        $trip_count = $this->trips()
                           ->inCompetition()
                           ->sum('total_points');

        $this->attributes['total_points'] = $trip_count;
    }

    public function setTotalSustainablePointsAttribute()
    {
        $sustainable_count = $this->trips()
                           ->inCompetition()
                           ->sum('total_sustainable_points');

        $this->attributes['total_sustainable_points'] = $sustainable_count;
    }

    public function setPercentSustainableTripsAttribute($value = 0)
    {
        // sustainable modes: walk, bike, scooter, park, wheelchair
        $total_sustainable_trips = $this->walk + $this->bike + $this->scooter + $this->park + $this->wheelchair;

        // catch division by zero
        if ($total_sustainable_trips != 0) {
            // calculate and format to 1 decimal place
            $value = number_format($total_sustainable_trips / $this->total_trips * 100, 1);
        }

        // set the attribute
        $this->attributes['percent_sustainable_trips'] = $value;
    }

    // TODO remove
    public function setPercentParticipationAttribute()
    {
        // number of days any class has logged a trip
        /*
         * select
         * number of trips
         * where
         * classroom is in school
         * trip date is after start
         * trip date is before end
         *
         */

        $value = 0;
        $total_trips = $this->total_trips;
        $max_trips = $this->number_of_pupils * app('competition')->duration;
        if($total_trips != 0 && $max_trips != 0){
            $value = $total_trips / $max_trips * 100;
        }

        $this->attributes['participation'] = $value;

        /*$trip_count           = $this->trips()->inCompetition()->count();
        $competition_duration = app('competition')->duration;

        $this->attributes['participation'] = $trip_count / $competition_duration;*/
    }

    //endregion

    public static function selectYearGroup()
    {
        $classroom = new Classroom;

        return pleaseSelect($classroom->yearGroup);
    }


}
