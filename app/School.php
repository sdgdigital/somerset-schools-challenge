<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

//use App\Scopes\SortScope;
use App\Events\SchoolSaved;

class School extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'local_authority_id',
        'walk',
        'bus',
        'bike',
        'car',
        'train',
        'scooter',
        'share',
        'park',
        'taxi',
        'wheelchair',
        'total_trips',
        'total_points',
        'total_sustainable_points',
        'percent_sustainable_trips',
        'percent_participation',
        'score',
        'postcode',
        'eastings',
        'northings',
        'number_of_pupils',
        'number_of_classes',
        'phase_of_education',
        'number_of_pt_stops_within_400_meters',
        'pt_service_frequency_7am_to_830am',
        'rank_for_pt_stops',
        'rank_for_frequency',
        'combined_rank',
        'overall_rank',
        'accessibility_marker',
        'notes',
    ];

    protected $hidden = [
        'walk',
        'bus',
        'bike',
        'car',
        'train',
        'scooter',
        'share',
        'park',
        'taxi',
        'wheelchair',
        'postcode',
        'eastings',
        'northings',
        'number_of_pupils',
        'number_of_classes',
        'phase_of_education',
        'number_of_pt_stops_within_400_meters',
        'pt_service_frequency_7am_to_830am',
        'rank_for_pt_stops',
        'rank_for_frequency',
        'combined_rank',
        'overall_rank',
        'accessibility_marker',
        'notes',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $appends = [
        'modes',
        'most',
        'least',
        'trips',
        'classes',
    ];

    //region Events

    public static function boot()
    {
        parent::boot();

        static::saving(function ($school) {
            $school->walk                      = 0;
            $school->bus                       = 0;
            $school->bike                      = 0;
            $school->car                       = 0;
            $school->train                     = 0;
            $school->scooter                   = 0;
            $school->share                     = 0;
            $school->park                      = 0;
            $school->taxi                      = 0;
            $school->wheelchair                = 0;
            $school->total_trips               = 0;
            $school->total_points              = 0;
            $school->total_sustainable_points  = 0;
            $school->percent_sustainable_trips = 0;
            $school->percent_participation     = 0;
            $school->score                     = 0;
            $school->number_of_classes         = 0;
            $school->number_of_pupils          = 0;
        });

        static::saved(function ($school) {
            event(new SchoolSaved($school));
        });

    }

    //endregion

    //region Relations

    public function local_authority()
    {
        return $this->belongsTo(LocalAuthority::class);
    }

    public function teachers()
    {
        return $this->hasMany(User::class);
    }

    public function classrooms()
    {
        return $this->hasMany(Classroom::class);
    }

    //endregion

    //region Accessors

    public function getModesAttribute()
    {
        $array = [];
        foreach (app('modes') as $mode) {
            $array[$mode->id] = $this->{$mode->class_name};
        }

        return $array;
    }

    public function getMostAttribute()
    {
        $array = [];
        foreach (app('modes') as $mode) {
            $array[$mode->id] = $this->{$mode->class_name};
        }

        // sort the values
        arsort($array);

        // get the key for the top mode
        $key = array_keys($array)[0];

        return $key;
    }

    public function getLeastAttribute()
    {
        $array = [];
        foreach (app('modes') as $mode) {
            $array[$mode->id] = $this->{$mode->class_name};
        }

        // sort the values
        asort($array);

        // get the key for the top mode
        $key = array_keys($array)[0];

        return $key;
    }

    public function getTripsAttribute()
    {
        return $this->total_trips;
    }

    public function getClassesAttribute()
    {
        return $this->number_of_classes;
    }

    //endregion

    //region Mutators

    public function setWalkAttribute()
    {
        $mode       = 'walk';
        $trip_count = $this->classrooms()
                           ->sum($mode);

        $this->attributes[$mode] = $trip_count;
    }

    public function setBusAttribute()
    {
        $mode       = 'bus';
        $trip_count = $this->classrooms()
                           ->sum($mode);

        $this->attributes[$mode] = $trip_count;
    }

    public function setBikeAttribute()
    {
        $mode       = 'bike';
        $trip_count = $this->classrooms()
                           ->sum($mode);

        $this->attributes[$mode] = $trip_count;
    }

    public function setCarAttribute()
    {
        $mode       = 'car';
        $trip_count = $this->classrooms()
                           ->sum($mode);

        $this->attributes[$mode] = $trip_count;
    }

    public function setTrainAttribute()
    {
        $mode       = 'train';
        $trip_count = $this->classrooms()
                           ->sum($mode);

        $this->attributes[$mode] = $trip_count;
    }

    public function setScooterAttribute()
    {
        $mode       = 'scooter';
        $trip_count = $this->classrooms()
                           ->sum($mode);

        $this->attributes[$mode] = $trip_count;
    }

    public function setShareAttribute()
    {
        $mode       = 'share';
        $trip_count = $this->classrooms()
                           ->sum($mode);

        $this->attributes[$mode] = $trip_count;
    }

    public function setParkAttribute()
    {
        $mode       = 'park';
        $trip_count = $this->classrooms()
                           ->sum($mode);

        $this->attributes[$mode] = $trip_count;
    }

    public function setTaxiAttribute()
    {
        $mode       = 'taxi';
        $trip_count = $this->classrooms()
                           ->sum($mode);

        $this->attributes[$mode] = $trip_count;
    }

    public function setWheelchairAttribute()
    {
        $mode       = 'wheelchair';
        $trip_count = $this->classrooms()
                           ->sum($mode);

        $this->attributes[$mode] = $trip_count;
    }

    public function setTotalTripsAttribute()
    {
        $trip_count = $this->classrooms()
                           ->sum('total_trips');

        $this->attributes['total_trips'] = $trip_count;
    }

    public function setTotalPointsAttribute()
    {
        $trip_count = $this->classrooms()
                           ->sum('total_points');

        $this->attributes['total_points'] = $trip_count;
    }

    public function setTotalSustainablePointsAttribute()
    {
        $sustainable_count = $this->classrooms()
                           ->sum('total_sustainable_points');

        $this->attributes['total_sustainable_points'] = $sustainable_count;
    }

    public function setPercentSustainableTripsAttribute($value = 0)
    {
        // sustainable modes: walk, bike, scooter, park, wheelchair
        $total_sustainable_trips = $this->walk + $this->bike + $this->scooter + $this->park + $this->wheelchair;

        // catch division by zero
        if ($total_sustainable_trips != 0) {
            // calculate and format to 1 decimal place
            $value = number_format($total_sustainable_trips / $this->total_trips * 100, 1);
        }

        // set the attribute
        $this->attributes['percent_sustainable_trips'] = $value;
    }

    public function setPercentParticipationAttribute($value = 0)
    {
        $total_trips = $this->total_trips;
        $max_trips = $this->number_of_pupils * app('competition')->duration;

        if ($total_trips != 0 && $max_trips != 0) {
            $value = $total_trips / $max_trips * 100;
        }

        /*$number_of_trips = Trip::where('date', '>=', app('competition')->start_date)->where('date', '<=', app('competition')->end_date)->get()->groupBy('classroom_id')->count();
        $competition_duration = app('competition')->duration;

        if ($number_of_trips != 0 && $competition_duration != 0) {
            $value = $number_of_trips / $competition_duration;
        }*/

        $this->attributes['percent_participation'] = $value;
    }

    public function setScoreAttribute($value = 0)
    {
        $points = $this->total_points;
        $sustainable_points = $this->total_sustainable_points;
        $participation = $this->percent_participation;

        if ($points != 0 && $sustainable_points != 0 && $participation != 0) {
            // gets (sustainable percentage from total points) then apply participation factor to produce score
            // score uses a 10 multiplier to increase numerical digits
            $value = round((($sustainable_points / $points * 100) / 100 * $participation) * 10, 2);
        }

        $this->attributes['score'] = $value;

        /*$points = $this->total_points;
        $pupils = $this->number_of_pupils;
        $participation = $this->percent_participation;

        if ($points != 0 && $pupils != 0 && $participation != 0) {
            $value = round($points / $pupils * $participation * 100, 2);
        }

        $this->attributes['score'] = $value;*/
    }

    public function setNumberOfPupilsAttribute()
    {
        $this->attributes['number_of_pupils'] = $this->classrooms()->sum('number_of_pupils');
    }

    public function setNumberOfClassesAttribute()
    {
        $this->attributes['number_of_classes'] = $this->classrooms()->count();
    }

    //endregion

    //region Scopes

    public function scopeInLocalAuthority($query)
    {
        if (request()->has('local_authority') && ! empty(request()->input('local_authority'))) {
            $query = $query->where('local_authority_id', '=', request()->input('local_authority'));
        }

        return $query;
    }

    //endregion

    //region Static

    public static function selectList()
    {
        // set up the query
        $query = School::query();

        // run the query
        $collection = $query
            ->orderBy('name', 'asc')
            ->get();

        // prepare the list
        $selectList = [];
        foreach ($collection as $model) {

            // build the string
            $selectList[$model->id] = $model->name;
        }

        // add the 'Please select' option
        $selectList = pleaseSelect($selectList);

        return $selectList;
    }

    //endregion

}
