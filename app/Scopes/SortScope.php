<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class SortScope implements Scope
{
  /**
   * Apply the scope to a given Eloquent query builder.
   *
   * @param  \Illuminate\Database\Eloquent\Builder $builder
   * @param  \Illuminate\Database\Eloquent\Model $model
   * @param array $attributes
   */
  public function apply(Builder $builder, Model $model)
  {

    // apply any sorting
    if (request()->has('sort') && !empty($this->attributes)) {

      // split the querystring
      $parts = explode('-', request()->input('sort'));

      if (in_array($parts[0], $this->attributes)) {
        $builder->orderBy($parts[0], $parts[1]);
      } else {
        $builder->orderBy($this->attributes[0], 'asc');
      }

    } else {
      $builder->orderBy($this->attributes[0], 'asc');
    }

  }
}