<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Login;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogSuccessfulLogin
{
  /**
   * Create the event listener.
   *
   * @return void
   */
  public function __construct()
  {
    //
  }

  /**
   * Handle the event.
   *
   * @param  Login $event
   * @return void
   */
  public function handle(Login $event)
  {
    $user = $event->user;

    $user->last_login = new \DateTime;
    $user->last_login_ip = request()->getClientIp();
    $user->save();
  }
}
