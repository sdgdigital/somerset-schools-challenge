<?php

namespace App\Listeners;

use App\Events\ClassroomSaved;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\School;

class UpdateSchool
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ClassroomSaved $event
     *
     * @return void
     */
    public function handle(ClassroomSaved $event)
    {
        if ( ! empty($event->classroom)) {

            // inspect the relation
            $school_id = $event->classroom->school_id;

            // grab the school
            $school = School::find($school_id);

            // cascade the event
            if ( ! empty($school)) {
                $school->save();
            }
        }
    }
}
