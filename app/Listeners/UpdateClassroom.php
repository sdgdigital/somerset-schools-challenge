<?php

namespace App\Listeners;

use App\Events\TripSaved;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Classroom;

class UpdateClassroom
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TripSaved $event
     *
     * @return void
     */
    public function handle(TripSaved $event)
    {
        if ( ! empty($event->trip)) {

            // inspect the relation
            $classroom_id = $event->trip->classroom_id;

            // grab the classroom
            $classroom = Classroom::find($classroom_id);

            // cascade the save event
            if ( ! empty($classroom)) {
                $classroom->save();
            }
        }
    }
}
