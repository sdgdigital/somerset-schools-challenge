<?php namespace App\ViewComposers;

use Illuminate\Contracts\View\View;

class MasterViewComposer {

  /**
   * Create a new master composer.
   *
   */
  public function __construct()
  {

  }

  /**
   * Bind data to the view.
   *
   * @param  View  $view
   * @return void
   */
  public function compose(View $view)
  {
    if (!app()->runningInConsole()) {
      $view->with('options', app('options'));
      $view->with('weather', app('weather'));
      $view->with('competition', app('competition'));
      $view->with('modes', app('modes'));
      $view->with('now', app('now'));
      $view->with('today', app('today'));
      $view->with('tomorrow', app('tomorrow'));

      if (auth()->check()) {
        $view->with('user', app('user'));
      }

    }
  }
}