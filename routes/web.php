<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

// Authentication
Route::get(str_slug(trans('app.login')), 'Auth\LoginController@showLoginForm')->name('login');
Route::post(str_slug(trans('app.login')), 'Auth\LoginController@login');
Route::post(str_slug(trans('app.logout')), 'Auth\LoginController@logout')->name('logout');

// Registration
Route::get(str_slug(trans('app.register')), 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post(str_slug(trans('app.register')), 'Auth\RegisterController@register');

// Password Reset
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

// Application
Route::group(['middleware' => 'auth'], function () {

    Route::get('dashboard', 'DashboardController@index')->name('dashboard');

    // Base views
    Route::get('add-journeys', 'App\ViewController@index')->name('add-journeys');
    Route::get('compare', 'App\ViewController@index')->name('compare');
    Route::get('leaderboard', 'App\ViewController@index')->name('leaderboard');

    // User
    Route::get('my-account', 'MyAccountController@getAccount')->name('my-account.edit');
    Route::post('my-account', 'MyAccountController@postAccount')->name('my-account.update');
//  Route::get('my-classroom', 'MyAccountController@getClassroom')->name('my-classroom.edit');

    // App
    Route::group(['prefix' => 'app'], function () {

        Route::get('trips', 'App\JourneysController@index');
        Route::post('trips', 'App\JourneysController@index');

        Route::get('leaderboard', 'App\LeaderboardController@index');

        Route::get('compare/class', 'App\CompareController@classroom');
        Route::get('compare/year', 'App\CompareController@year');
        Route::get('compare/school', 'App\CompareController@school');

    });

    // Admin
    Route::group(['prefix' => 'admin'], function () {

        Route::get('options', 'Admin\OptionController@getOptions')->name('options.index');
        Route::post('options', 'Admin\OptionController@postOptions')->name('options.update');

        Route::resource('competitions', 'Admin\CompetitionsController');
        Route::resource('modes', 'Admin\ModesController', ['except' => ['create', 'destroy']]);

        Route::resource('local-authorities', 'Admin\LocalAuthoritiesController');
        Route::resource('schools', 'Admin\SchoolsController');
        Route::resource('classrooms', 'Admin\ClassroomsController');
        Route::resource('users', 'Admin\UsersController');
        Route::resource('pages', 'Admin\PagesController', ['only' => ['index', 'edit', 'update']]);

        Route::get('reports', 'Admin\ReportController@getIndex')->name('reports.index');
        Route::get('reports/users', 'Admin\ReportController@getUsersReport')->name('reports.users');
        Route::get('reports/schools', 'Admin\ReportController@getSchoolsReport')->name('reports.schools');
        Route::get('reports/trips', 'Admin\ReportController@getTripsReport')->name('reports.trips');
        Route::get('reports/year-group', 'Admin\ReportController@getYearGroupReport')->name('reports.year-group');

        Route::resource('trips', 'Admin\TripsController');

        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    });

});

Route::get('how-it-works', ['uses' => 'CustomPageController@getPage'])->name('how-it-works');
Route::get('terms-and-conditions', ['uses' => 'CustomPageController@getPage'])->name('terms-and-conditions');
Route::get('privacy-statement', ['uses' => 'CustomPageController@getPage'])->name('privacy-statement');

// Public
Route::get('leaderboard-public', 'App\ViewController@index')->name('leaderboard-public');
Route::group(['prefix' => 'app'], function () {
    Route::get('leaderboard-public', 'App\LeaderboardController@index');
});